package cat.itb.joanpuigcerver.dam.m03.uf4.exercises.quiz;

import java.util.Scanner;

public class FreeTextQuestion implements Question{
    private String question;
    private String answer;
    private String userAnswer;

    public FreeTextQuestion(String question, String answer) {
        this.question = question;
        this.answer = answer;
        this.userAnswer = "";
    }

    @Override
    public void printQuestion() {
        System.out.println(question);
    }

    @Override
    public void askAnswer(Scanner scanner) {
        this.userAnswer = scanner.nextLine();
    }

    @Override
    public boolean isCorrect() {
        return userAnswer.equals(answer);
    }
}
