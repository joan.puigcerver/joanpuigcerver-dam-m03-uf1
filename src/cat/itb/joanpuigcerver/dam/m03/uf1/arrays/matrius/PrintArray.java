package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.matrius;

import java.awt.Color;
import java.awt.Font;

public class PrintArray {
    final double border_width = 0.001;
    
    final Color[] colors = {
            Color.WHITE ,
            Color.BLUE, 
            Color.YELLOW, 
            Color.RED, 
            Color.GREEN,
            Color.ORANGE,
            Color.CYAN,
            Color.MAGENTA,
            Color.PINK,
            Color.LIGHT_GRAY,
            Color.BLACK
            };
    Color emptyColor = Color.WHITE;
    Color fillColor = Color.BLUE;
    
    int min = 0;
    int max = 10;
    int presicion = 100;
    
    boolean transpose = true;
    
    int [][] currentStatus;
    boolean drawText = false;
    boolean drawColor = false;
    boolean colorInterpolation = false;
    
    private long previousTime = 0;

    double boxWidth;
    double boxHeigth;

    /**
     * Constructor de la classe.
     * 
     * @param width Amplada en píxels de la finestra
     * @param height Altura en píxels de la finestra
     */
    public PrintArray( int width, int height )
    {
        StdDraw.setCanvasSize(width, height);
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 1);
        StdDraw.setYscale(0, 1);
        
        currentStatus = new int[1][1];
        updateView();
    }
    
    /**
     * Constructor per defecte.
     * Les dimensions de la finestra per defecte son 512x512
     */
    public PrintArray()
    {
        this(512,512);
    }
    
    /**
     * Mostra i actualitza la finestra.
     */
    public void show()
    {
        StdDraw.show();
    }
    
    /**
     * Aquest paràmetre determina el tamany de la font en la que s'imprimiran els nombres
     * del array.
     *  
     * @param size
     */
    public void setFontSize(int size)
    {
        StdDraw.setFont(new Font("SansSerif", Font.PLAIN, size));
    }
    
    /**
     * Aquest paràmetre determina si s'imprimiran els diferents nombres
     * presents en l'array.
     *  
     * @param drawText
     */
    public void setDrawText(boolean drawText)
    {
        this.drawText = drawText;
    }
    
    /**
     * Aquest paràmetre determina si s'imprimiran diferents colors per als diferents nombres
     * presents en l'array.
     * 
     * Els colors estan predefinits a l'array colors[]
     * 
     * @param drawColor
     */
    public void setDrawColor(boolean drawColor)
    {
        this.drawColor = drawColor;
    }
    
    /**
     * Aquest paràmetre determina si es realitza una interpolació del color en funció del valor
     * de l'array.
     * El valor min (defecte 0)  correspon al color emptyColor (per defecte el blanc)
     * El valor max (defecte 10) correspon al color fillColor  (per defecte el blau)
     * 
     * @param colorInterpolation
     */
    public void setColorInterpolation(boolean colorInterpolation)
    {
        this.colorInterpolation = colorInterpolation;
    }
    
    /**
     * Aquest paràmetre determina si es realitza una interpolació del color en funció del valor
     * de l'array.
     * El valor min (defecte 0)  correspon al color emptyColor (per defecte el blanc)
     * El valor max (defecte 10) correspon al color fillColor  (per defecte el blau)
     * 
     * @param colorInterpolation
     */
    public void setColorInterpolation(boolean colorInterpolation, int min, int max)
    {
        setColorInterpolation(colorInterpolation);
        this.min = min;
        this.max = max;
    }
    
    /**
     * Aquest paràmetre el color quan una casella no és 0 o el color màxim si s'utilitza interpolació.
     * 
     * @param color
     */
    public void setFillColor( Color color )
    {
        this.fillColor = color;
    }
    
    /**
     * Aquest paràmetre el color quan una casella és 0 o el color mínim si s'utilitza interpolació.
     * 
     * @param color
     */
    public void setEmptyColor( Color color )
    {
        this.emptyColor = color;
    }
    
    /**
     * Mostra un array 1-dimensional
     * 
     * @param array
     */
    public void printArray(int [] array) 
    {
        int[][] newArray = new int[array.length][1];
        for( int i = 0; i < array.length; i++)
            newArray[i][0] = array[i];
        printArray(newArray);
    }

    /**
     * Mostra un array 2-dimensional
     * 
     * @param array
     */
    public void printArray(int [][] array)
    {
        int xSize = currentStatus.length;
        int ySize = currentStatus[0].length;
        
        if( transpose )
        {
            int newXSize = array[0].length;
            int newYSize = array.length;
            int[][] newarray = new int[newXSize][newYSize];
            for( int i = 0; i < newYSize; i++)
            {
                for( int j = 0; j < newXSize; j++)
                {
                    newarray[j][i] = array[i][j];
                }
            }
            currentStatus = newarray;
        }
        else
        {
            currentStatus = array;
        }
        
        if( xSize != currentStatus.length || ySize != currentStatus[0].length)
        {
            updateView();
        }
        drawCells();
        drawGrid();
        show();     
    }
    
    /**
     * Atura el programa els milisegons indicats.
     * 
     * @param array
     */
    public void sleep(long ms)
    {
        try
        {
            //long current = System.currentTimeMillis();
            //long diff = previousTime == 0 ? 0 : current - current;
            //Thread.sleep(ms - diff);
            Thread.sleep(ms);
        }
        catch( Exception e ) {System.err.println("Thread.sleep error!");}
    }

    private void updateView()
    {
        boxWidth = 1.0 / currentStatus.length;
        boxHeigth = 1.0 / currentStatus[0].length;
    }
    
    private String intToString(int n)
    {
        int n_aux = n;
        int count = 1;
        
        while( n_aux > 10)
        {
            n_aux /= 10;
            count++;
        }
        return String.format("%"+count+"d", n);
    }
    
    private Color color_interpolation( int n, int min, int max, Color minColor, Color maxColor )
    {
        if( n >= max ) return maxColor;
        if( n <= min ) return minColor;
        
        int[] color_min = { minColor.getRed(),
                            minColor.getGreen(),
                            minColor.getBlue() };
        int[] color_max = { maxColor.getRed(),
                            maxColor.getGreen(),
                            maxColor.getBlue() };
        double proportion = (double)(n - min) / (double)(max-min);
        
        int[] new_color = new int[3];
        for( int i = 0; i < new_color.length; i++ )
        {
            new_color[i] = color_min[i] + (int)(proportion * (double)   (color_max[i] - color_min[i]));
            
        }
        return new Color(new_color[0], new_color[1], new_color[2]);
    }
    
    private Color getColor( int value )
    {
        if( colorInterpolation )
        {
            return color_interpolation(value, min, max, emptyColor, fillColor);
        }
        if( drawColor && value > 0 && value <= (colors.length -1) * presicion)
        {
            int color = (value -1) / presicion +1;
            int grad = (value -1) % presicion +1;
            return color_interpolation(grad, 0, presicion, emptyColor, colors[color]);
        }
        else
            if( value == 0)
                return emptyColor;
            else
                return fillColor;
    }
    
    private void drawCells()
    {
        // (currentStatus[0].length - 1 - j) Per donar-li la volta al sistema de coordenades.
        for (int i = 0; i < currentStatus.length; i++) {
            for (int j = 0; j < currentStatus[0].length; j++) {
                StdDraw.setPenColor(getColor(currentStatus[i][j]));
                StdDraw.filledRectangle(i * boxWidth + boxWidth/2,
                                       (currentStatus[0].length - 1 - j) * boxHeigth + boxHeigth/2,
                                        boxWidth/2,
                                        boxHeigth/2);
                if( drawText )
                {
                    StdDraw.setPenColor(Color.BLACK);
                    StdDraw.text(i * boxWidth + boxWidth/2,
                                (currentStatus[0].length - 1 - j) * boxHeigth + boxHeigth/2,
                                intToString(currentStatus[i][j]));
                }
            }
        }
        previousTime = System.currentTimeMillis();
    }

    private void drawGrid()
    {
        StdDraw.setPenColor(Color.BLACK);
        for (int i = 0; i <= currentStatus.length ; i++)
            StdDraw.filledRectangle(i * boxWidth ,
                    0.5,
                    border_width,
                    1);
        for (int j = 0; j <= currentStatus[0].length ; j++)
            StdDraw.filledRectangle(0.5,
                    j * boxHeigth ,
                    1,
                    border_width);
    }
}