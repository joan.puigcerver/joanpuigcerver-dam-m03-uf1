package cat.itb.joanpuigcerver.dam.m03.uf4.exercises;

public class MovingMechanicalArmApp {
    public static void main(String[] args) {
        MovingMechanicalArm arm = new MovingMechanicalArm();
        System.out.println(arm);
        arm.turnOn();
        System.out.println(arm);
        arm.updateHeight(3);
        System.out.println(arm);
        arm.updateAngle(180);
        System.out.println(arm);
        arm.updateHeight(-23);
        System.out.println(arm);
        arm.updateAngle(580);
        System.out.println(arm);
        arm.updateHeight(3);
        System.out.println(arm);
        arm.move(4.5);
        System.out.println(arm);
        arm.move(-100);
        System.out.println(arm);
        arm.turnOff();
        System.out.println(arm);
        arm.updateHeight(3);
        System.out.println(arm);
    }
}
