package cat.itb.joanpuigcerver.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;

public class Camping {
    private List<Reserva> reserves;

    public Camping(){
        reserves = new ArrayList<Reserva>();
    }

    public void addReserva(Reserva r){
        reserves.add(r);
    }
    public void addReserva(String nom, int nombrePersones){
        Reserva r = new Reserva(nom, nombrePersones);
        reserves.add(r);
    }

    public int getNombreParceles(){
        return this.reserves.size();
    }

    public int getNombrePersones(){
        int total = 0;
        for(Reserva r : reserves){
            total += r.getNumeroPersones();
        }
        return total;
    }

    public void printInfo(){
        System.out.printf("parcel·les: %d\n", getNombreParceles());
        System.out.printf("persones: %d\n", getNombrePersones());
    }

    public void removeReserva(String nom){
        int i;
        boolean found = false;
        for(i = 0; i < reserves.size(); i++){
            Reserva r = reserves.get(i);
            if(r.getNomReserva().equals(nom)){
                found = true;
                break;
            }
        }

        if(found){
            reserves.remove(i);
        }
    }
}
