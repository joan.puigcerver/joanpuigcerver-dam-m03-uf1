package cat.itb.joanpuigcerver.dam.m03.uf1.mix;

import java.util.Locale;
import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String linia = scanner.nextLine();
        linia = linia.replace(".", "");
        linia = linia.replace(",", "");
        linia = linia.replace("'", "");
        linia = linia.replace("!", "");
        linia = linia.replace("?", "");
        linia = linia.replace("-", "");
        linia = linia.replace(" ", "");
        linia = linia.toLowerCase();
        System.out.println(linia);

        boolean esPalindrom = true;
        for(int i = 0, j = linia.length() - 1; i <= j; i++, j--){
            if(linia.charAt(i) != linia.charAt(j))
                esPalindrom = false;
        }

        System.out.println(esPalindrom);
    }
}