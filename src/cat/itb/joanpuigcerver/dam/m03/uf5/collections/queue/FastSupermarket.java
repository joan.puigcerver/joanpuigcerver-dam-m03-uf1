package cat.itb.joanpuigcerver.dam.m03.uf5.collections.queue;

import java.util.*;

public class FastSupermarket {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        PriorityQueue<Customer> queue = new PriorityQueue<Customer>(Comparator.comparing(Customer::getProductCount));

        while(true){
            String name = scanner.next();
            if(name.equalsIgnoreCase("END"))
                break;
            else if (name.equalsIgnoreCase("NEXT")){
                Customer next = queue.poll();
                if(next == null)
                    System.out.println("No hi ha clients a la cua");
                else
                    System.out.println("Next: " + next);
            }else {
                int productCount = scanner.nextInt();
                queue.add(new Customer(name, productCount));
            }

            System.out.println(queue);
        }
    }
}
