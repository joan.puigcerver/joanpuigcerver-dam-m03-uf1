package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class isValidNote {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int valorBillet = scanner.nextInt();

        boolean isValidNote = valorBillet == 5
                || valorBillet == 10
                || valorBillet == 20
                || valorBillet == 50
                || valorBillet == 100
                || valorBillet == 200
                || valorBillet == 500;

        System.out.println(isValidNote);
    }
}
