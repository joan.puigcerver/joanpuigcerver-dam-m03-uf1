package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data;

public class Equip {
    private String nom;
    private String abreviacio;
    private int id;


    public Equip(int id, String nom, String abreviacio) {
        this.id = id;
        this.nom = nom;
        this.abreviacio = abreviacio;
    }

    public String getNom() {
        return nom;
    }
    public String getAbreviacio() {
        return abreviacio;
    }
    public int getId() {
        return id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setAbreviacio(String abreviacio) {
        this.abreviacio = abreviacio;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)",this.nom, this.abreviacio);
    }
}
