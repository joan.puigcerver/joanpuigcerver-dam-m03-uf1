package cat.itb.joanpuigcerver.dam.m03.uf2.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class EndangeredAnimalSights {
    public static int countTotal(Avistament[] avistaments, Animal a){
        int suma = 0;
        for(Avistament av : avistaments){
            if(av.getAnimal().getNomCientific().equals(a.getNomCientific()))
                suma += av.getNombre();
        }
        return suma;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nombreAnimals = scanner.nextInt();
        scanner.nextLine();

        Animal[] animals = new Animal[nombreAnimals];
        for(int i = 0; i < nombreAnimals; i++){
            String nomCientific = scanner.nextLine();
            String nomComu = scanner.nextLine();
            animals[i] = new Animal(nomComu, nomCientific);
        }

        int nombreAvistaments = scanner.nextInt();
        scanner.nextLine();

        Avistament[] avistaments = new Avistament[nombreAvistaments];
        for(int i = 0; i < nombreAvistaments; i++){
            int posicioAnimal = scanner.nextInt();
            int compteAnimals = scanner.nextInt();
            avistaments[i] = new Avistament(animals[posicioAnimal], compteAnimals);
        }

        System.out.println("--- Avistaments ---");
        for(Avistament av : avistaments){
            System.out.println(av);
        }
        System.out.println("--- Resum ---");
        for(Animal a : animals){
            System.out.printf("%s: %d\n", a, countTotal(avistaments, a));
        }
    }
}
