package cat.itb.joanpuigcerver.dam.m03.uf6.generalexam.data;

import cat.itb.joanpuigcerver.dam.m03.uf5.generalexam.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDAO {
    public boolean insert(Book b){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO book(title, author, isbn, year, pages) VALUES(?, ?, ?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, b.getTitle());
            insertStatement.setString(2, b.getAuthor());
            insertStatement.setString(3, b.getIsbn());
            insertStatement.setInt(4, b.getYear());
            insertStatement.setInt(5, b.getPages());
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public List<Book> listBookByYear(int year){
        List<Book> list = new ArrayList<>();
        try{
            Database db = Database.getInstance();
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM book WHERE year = ?";
            PreparedStatement listStatement = connection.prepareStatement(query);
            listStatement.setInt(1, year);
            ResultSet resultat = listStatement.executeQuery();

            while (resultat.next()){
                String title = resultat.getString("title");
                String author = resultat.getString("author");
                String isbn = resultat.getString("isbn");
                int bookYear = resultat.getInt("year");
                int pages = resultat.getInt("pages");
                list.add(new Book(title, author, isbn, pages, bookYear));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }
    public Book getLongestBook(){
        try{
            Database db = Database.getInstance();
            Connection connection = Database.getInstance().getConnection();
            // String query = "SELECT * FROM book WHERE pages = (SELECT MAX(pages) from book)";
            String query = "SELECT * FROM book ORDER BY pages DESC LIMIT 1";
            Statement statement = connection.createStatement();
            ResultSet resultat = statement.executeQuery(query);

            if (resultat.next()){
                String title = resultat.getString("title");
                String author = resultat.getString("author");
                String isbn = resultat.getString("isbn");
                int bookYear = resultat.getInt("year");
                int pages = resultat.getInt("pages");
                return new Book(title, author, isbn, pages, bookYear);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
