package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.ui;

import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Equip;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Lliga;

public class MenuDades {
    MyScanner scanner;
    Lliga lliga;

    public MenuDades(MyScanner scanner){
        this.scanner = scanner;
        this.lliga = Lliga.getInstance();
    }

    public void mostrarMenu(){
        while(true) {
            System.out.println("Menu DADES");
            System.out.println("Quina operació vols realitzar?");
            System.out.println("1) Introduir equip");
            System.out.println("2) Llista equips");
            System.out.println("3) Introduir partit");
            System.out.println("4) Llista partits");
            System.out.println("0) Sortir");
            int accio = scanner.nextIntMax(4);

            switch (accio) {
                case 1:
                    introduirEquip();
                    break;
                case 2:
                    llistaEquips();
                    break;
                case 3:
                    introduirPartit();
                    break;
                case 4:
                    llistaPartits();
                    break;
                case 0:
                    return;
            }
        }
    }

    private void introduirEquip(){
        System.out.print("Introdueix el nom de l'equip: ");
        String nom = scanner.next();
        System.out.print("Introdueix l'abreviacio de l'equip: ");
        String abbreviacio = scanner.next();
        Equip e = lliga.addEquip(nom, abbreviacio);
        System.out.printf("El equip %s s'ha introduit correctament\n", e);
    }

    private void llistaEquips(){
        System.out.println("Llista equips:");
        lliga.llistaEquips();
    }

    private void introduirPartit(){
        lliga.llistaEquips();
        System.out.print("Introdueix l'equip local: ");
        int idLocal = scanner.nextIntMax(lliga.getNombreEquips());
        System.out.print("Introdueix l'equip visitant: ");
        int idVisitant = scanner.nextIntMax(lliga.getNombreEquips());
        System.out.print("Gols locals: ");
        int golsLocals = scanner.nextInt();
        System.out.print("Gols visitants: ");
        int golsVisitants = scanner.nextInt();
        lliga.addPartit(idLocal, idVisitant, golsLocals, golsVisitants);
    }

    private void llistaPartits(){
        System.out.println("Llista partits:");
        lliga.llistaPartits();
    }
}
