package cat.itb.joanpuigcerver.dam.m03.uf4.exercises;

import java.util.ArrayList;
import java.util.List;

public class InstrumentSimulator {
    public static void main(String[] args) {
        List<Instrument> instruments = new ArrayList<>();
        instruments.add(new Triangle(5));
        instruments.add(new Drum('A'));
        instruments.add(new Drum('O'));
        instruments.add(new Triangle(1));
        instruments.add(new Triangle(5));

        play(instruments);
    }

    private static void play(List<Instrument> instruments) {
        for(Instrument instrument: instruments){
            instrument.makeSound();
        }
    }
}