package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class InverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[] valors = new int[10];
        for(int i = 0; i < valors.length; i++)
            valors[i] = scanner.nextInt();

        for(int i = valors.length - 1; i >= 0; i--){
            System.out.print(valors[i]);
            System.out.print(" ");
        }
        System.out.println();
    }
}
