package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n = scanner.nextInt();

        int i = 1;
        while(i <= n){
            System.out.print(i);
            i++;
        }

        // Salt de línia final
        System.out.println();

    }
}
