package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String[] diesSetmana = {
                "dilluns"
                , "dimarts"
                , "dimecres"
                , "dijous"
                , "divendres"
                , "dissabte"
                , "diumenge"
        };

        String[] diesSetmanaSenseValors = new String[7];
        diesSetmanaSenseValors[0] = "dilluns";
        diesSetmanaSenseValors[1] = "dimarts";
        diesSetmanaSenseValors[2] = "dimecrest";
        diesSetmanaSenseValors[3] = "dijous";
        diesSetmanaSenseValors[4] = "divendres";
        diesSetmanaSenseValors[5] = "dissabte";
        diesSetmanaSenseValors[6] = "diumenge";

        int dia = scanner.nextInt();
        System.out.println(diesSetmana[dia]);
        System.out.println(diesSetmanaSenseValors[dia]);
    }
}
