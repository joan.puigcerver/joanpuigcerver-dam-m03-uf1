package cat.itb.joanpuigcerver.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class AvgTemperature {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        double average = IntegerLists.average(list);
        System.out.printf("Ha fet %.2f graus de mitjana\n", average);
    }
}
