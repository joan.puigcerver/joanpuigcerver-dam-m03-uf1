package cat.itb.joanpuigcerver.dam.m03.uf1.staticfunctions;

import java.util.Locale;
import java.util.Scanner;

public class HowBigIsMyPizzaRound {

    public static double calcularAreaRodona(double r){
        double area = Math.PI * Math.pow(r, 2);
        return area;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double diametre1 = scanner.nextDouble();
        double diametre2 = scanner.nextDouble();
        double diametre3 = scanner.nextDouble();
        double diametre4 = scanner.nextDouble();
        double diametre5 = scanner.nextDouble();

        double superficieRodona1 = calcularAreaRodona(diametre1/2);
        double superficieRodona2 = calcularAreaRodona(diametre2/2);
        double superficieRodona3 = calcularAreaRodona(diametre3/2);
        double superficieRodona4 = calcularAreaRodona(diametre4/2);
        double superficieRodona5 = calcularAreaRodona(diametre5/2);

        System.out.printf("Superficie rodona 1 (r: %.2f): %.2f\n", diametre1/2, superficieRodona1);
        System.out.printf("Superficie rodona 2 (r: %.2f): %.2f\n", diametre2/2, superficieRodona2);
        System.out.printf("Superficie rodona 3 (r: %.2f): %.2f\n", diametre3/2, superficieRodona3);
        System.out.printf("Superficie rodona 4 (r: %.2f): %.2f\n", diametre4/2, superficieRodona4);
        System.out.printf("Superficie rodona 5 (r: %.2f): %.2f\n", diametre5/2, superficieRodona5);

    }
}
