package cat.itb.joanpuigcerver.dam.m03.uf6.projecte.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class Menu {
    List<String> options;
    Scanner scanner;

    public Menu(Scanner scanner){
        this.scanner = scanner;
        this.options = new ArrayList<>();
    }

    protected void addOption(String option){
        options.add(option);
    }


    private int selectOption(){
        System.out.print("Introdueix l'opció: ");
        int n = scanner.nextInt();
        while (n < 0 && n > options.size()){
            System.err.println("Opció no vàlida.");
            System.out.print("Introdueix l'opció: ");
            n = scanner.nextInt();
        }
        return n;
    }

    public void showMenu(){
        while(true) {
            for (int i = 0; i < options.size(); i++)
                System.out.printf("%d) %s\n", i + 1, options.get(i));
            System.out.println("0) Sortir.");
            int selected = selectOption();
            if(selected == 0)
                break;
            else
                action(selected);
        }
    }

    public abstract void action(int selected);
}
