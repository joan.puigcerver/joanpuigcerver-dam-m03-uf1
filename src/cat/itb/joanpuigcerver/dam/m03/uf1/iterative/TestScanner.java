package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class TestScanner {
    public static void main(String[] args) {
        Scanner scaner = new Scanner(System.in).useLocale(Locale.US);

        String linia1 = scaner.nextLine();
        System.out.println("Linia1: " + linia1);

        int n1 = scaner.nextInt();
        System.out.println("N1: " + n1);

        String finalLinia = scaner.nextLine();
        System.out.println("Final linia: " + finalLinia);

        String linia2 = scaner.nextLine();
        System.out.println("Linia2: " + linia2);
    }
}




