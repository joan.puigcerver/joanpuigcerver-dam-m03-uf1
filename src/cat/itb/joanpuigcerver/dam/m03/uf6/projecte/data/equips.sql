CREATE TABLE equips(
    id SERIAL,
    nom VARCHAR(30),
    abbr VARCHAR(30),
    PRIMARY KEY(id)
);

CREATE TABLE partits(
    id SERIAL,
    id_local INT,
    gols_local INT,
    id_visitant INT,
    gols_visitant INT,
    PRIMARY KEY(id),
        CONSTRAINT fk_id_local FOREIGN KEY (id_local) REFERENCES equips(id) ON DELETE CASCADE,
        CONSTRAINT fk_id_visitant FOREIGN KEY (id_visitant) REFERENCES equips(id) ON DELETE CASCADE
);

/* select p.id, e.id as id_local, e.nom as nom_local, e.abbr as abbr_local, p.gols_local, v.id as id_visitant, v.nom as nom_visitant, v.abbr as abbr_visitant, p.gols_visitant 
from equips e, partits p, equips v where e.id = p.id_local and v.id = p.id_visitant; */
INSERT INTO equips(id, nom, abbr) values(1, 'València', 'VLC');
INSERT INTO equips(id, nom, abbr) values(2, 'Madrid', 'MAD');
INSERT INTO equips(id, nom, abbr) values(3, 'Barcelona', 'BCN');
INSERT INTO equips(id, nom, abbr) values(4, 'Màlaga', 'MAL');
INSERT INTO equips(id, nom, abbr) values(5, 'Vilareal', 'VIL');
INSERT INTO equips(id, nom, abbr) values(6, 'Betis', 'BTS');

INSERT INTO partits(id, id_local, gols_local, id_visitant, gols_visitant) values(1, 1, 3, 3, 0);
INSERT INTO partits(id, id_local, gols_local, id_visitant, gols_visitant) values(2, 2, 0, 2, 2);
INSERT INTO partits(id, id_local, gols_local, id_visitant, gols_visitant) values(3, 3, 2, 4, 2);
INSERT INTO partits(id, id_local, gols_local, id_visitant, gols_visitant) values(4, 4, 1, 5, 3);
INSERT INTO partits(id, id_local, gols_local, id_visitant, gols_visitant) values(5, 5, 4, 4, 1);
INSERT INTO partits(id, id_local, gols_local, id_visitant, gols_visitant) values(6, 6, 1, 1, 2);
