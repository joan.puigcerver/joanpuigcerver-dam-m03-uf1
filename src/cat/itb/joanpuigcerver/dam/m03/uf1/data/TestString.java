package cat.itb.joanpuigcerver.dam.m03.uf1.data;

public class TestString {
    public static void main(String[] args) {
        String dni = "12345678";
        char lletraDNI = 'A';
        String nom = "Joan";
        String cognom = "Puigcerver";
        double notaMitjana = 6.8475823;

        System.out.printf("Persona: %s-%c: %-10s %s (%.2f)\n", dni, lletraDNI, "Joan", cognom, notaMitjana);
        System.out.printf("Persona: %s-%c: %-10s %s (%.2f)\n", dni, lletraDNI, "Guillem", cognom, notaMitjana);
    }
}
