package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IsDivisible {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int primerValor = scanner.nextInt();
        int segonValor= scanner.nextInt();

        int residu = primerValor % segonValor;
        boolean isDivisible = residu == 0;

        System.out.println(residu);
        System.out.println(isDivisible);
    }
}
