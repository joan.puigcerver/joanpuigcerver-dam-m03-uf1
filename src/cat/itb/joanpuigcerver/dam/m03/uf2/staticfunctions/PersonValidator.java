package cat.itb.joanpuigcerver.dam.m03.uf2.staticfunctions;

import java.util.Scanner;
import java.util.Locale;

public class PersonValidator {
    /**
     * Returns true if is a valid phone number (composed only by digits, spaces or +)
     * @param phone
     * @return true if valid
     */
    public static boolean isValidPhoneNumber(String phone){
        // TODO
        return false;
    }

    /**
     * Returns true if is a valid person name (composed only characters and starts with upper case)
     * @param personName
     * @return true if valid
     */
    public static boolean isValidPersonName(String personName){
        if(personName.length() == 0)
            return false;

        char firstLetter = personName.charAt(0);
        // Comprovo si la primera lletra no és majúscula
        if(!Character.isUpperCase(firstLetter)){
            return false;
        }

        for (int i = 0; i < personName.length(); i++) {
            if(!Character.isLetter(personName.charAt(i))){
                return false;
            }
        }

        return true;
    }

    /**
     * Returns true if is a valid dni (including correct letter)
     * @param dni
     * @return true if valid
     */
    public static boolean isValidDni(String dni){
        // TODO
        return false;
    }

    /**
     * Returns true if is a valid postalCode
     * @param postalCode
     * @return true if valid
     */
    public static boolean isValidPostalcode(String postalCode){
        // TODO
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String nom = scanner.next();
        boolean isNomValid = isValidPersonName(nom);
        System.out.printf("El nom %s és valid? %s\n", nom, isNomValid);
    }
}

