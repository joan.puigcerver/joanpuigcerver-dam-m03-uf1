package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n;
        do {
            System.out.println("Introdueix un número entre 1 i 5:");
            n = scanner.nextInt();
        //} while (!(n >= 1 && n<= 5));
        } while (n < 1 || n > 5);

        System.out.println("Has introduït: " + n);
    }
}
