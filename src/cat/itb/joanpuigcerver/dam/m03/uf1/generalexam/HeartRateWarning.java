package cat.itb.joanpuigcerver.dam.m03.uf1.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class HeartRateWarning {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int minim = scanner.nextInt();
        int maxim = scanner.nextInt();

        int minimAcumulat = 0;
        int maximAcumulat = 0;
        int actual = scanner.nextInt();
        while(actual != -1){
            if(actual < minim)
                minimAcumulat++;
            else
                minimAcumulat = 0;

            if(actual > maxim)
                maximAcumulat++;
            else
                maximAcumulat = 0;

            if(maximAcumulat >= 3)
                System.out.println("MASSA ALT");
            if(minimAcumulat >= 3)
                System.out.println("MASSA BAIX");

            actual = scanner.nextInt();
        }
    }
}
