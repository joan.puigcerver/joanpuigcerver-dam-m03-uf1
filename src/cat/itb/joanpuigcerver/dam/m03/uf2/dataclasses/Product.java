package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import java.util.Scanner;

 public class Product{
     private String name;
     private double prize;
     private int count;

     public static Product readProduct(Scanner scanner){
         int count = scanner.nextInt();
         String name = scanner.next();
         double prize = scanner.nextDouble();
         return new Product(name, prize, count);
     }
     /**
      * Product constructor. Creates a product given the name i prize of the product
      * @param name Product name
      * @param prize Product prize
      */
     public Product(String name, double prize){
         this(name, prize, 1);
     }
     /**
      * Product constructor. Creates a product given the name i prize of the product
      * @param name Product name
      * @param prize Product prize
      * @param count Product count (how many I have)
      */
     public Product(String name, double prize, int count){
         this.name = name;
         this.prize = prize;
         this.count = count;
     }

     // Getters
     public String getName(){
          return this.name;
     }
     public double getPrize(){
         return this.prize;
     }

     // Setters
     public void setName(String name){
         this.name = name;
     }
     public void setPrize(double prize){
         this.prize = prize;
     }

     @Override
     public String toString(){
         return String.format("%d %s (%.2f€)", count, name, prize);
     }

     public double totalPrize(){
         return count * prize;
     }
 }

