package cat.itb.joanpuigcerver.dam.m03.uf4.exercises.quiz;

import java.util.Scanner;

public class MultipleChoiceQuestion implements Question{
    private String question;
    private String[] options;
    private int answer;
    private int userAnswer;

    public MultipleChoiceQuestion(String question, String[] options, int answer) {
        this.question = question;
        this.options = options;
        this.answer = answer;
        this.userAnswer = -1;
    }

    @Override
    public void printQuestion() {
        System.out.println(question);
        for (int i = 0; i < options.length; i++)
            System.out.printf("%d. %s\n", i+1, options[i]);
    }

    @Override
    public void askAnswer(Scanner scanner) {
        this.userAnswer = scanner.nextInt();
    }

    @Override
    public boolean isCorrect() {
        return userAnswer == answer;
    }
}
