package cat.itb.joanpuigcerver.dam.m03.uf6.generalexam;

import cat.itb.joanpuigcerver.dam.m03.uf5.generalexam.Book;
import cat.itb.joanpuigcerver.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.joanpuigcerver.dam.m03.uf6.generalexam.data.Database;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class LongestBookApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Database db = Database.getInstance();
        BookDAO bookDAO = new BookDAO();
        db.connect();

        Book longest = bookDAO.getLongestBook();
        System.out.println(longest);

        db.close();
    }
}
