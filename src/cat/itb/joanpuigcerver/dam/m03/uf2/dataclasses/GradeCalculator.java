package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class GradeCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int numeroNotes = scanner.nextInt();
        StudentGrade[] notes = new StudentGrade[numeroNotes];

        for (int i = 0; i < notes.length; i++) {
            notes[i] = StudentGrade.readStudent(scanner);
        }

        for (int i = 0; i < notes.length; i++) {
            System.out.println(notes[i]);
            System.out.printf("%s: %.1f\n", notes[i].getNom(), notes[i].getNotaFinal());
        }
    }
}
