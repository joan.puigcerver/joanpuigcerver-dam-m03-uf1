package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class AbsoluteValue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int value = scanner.nextInt();

        int absoluteValue = 0;
        if(value > 0)
            absoluteValue = value;
        else
            absoluteValue = -value;

        System.out.println(absoluteValue);
    }
}
