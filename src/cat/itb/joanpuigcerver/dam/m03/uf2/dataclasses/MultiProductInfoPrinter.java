package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class MultiProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nombreProductes = scanner.nextInt();
        Product[] productes = new Product[nombreProductes];

        for (int i = 0; i < productes.length; i++) {
            String nom = scanner.next();
            double preu = scanner.nextDouble();
            productes[i] = new Product(nom, preu);
        }

        for (int i = 0; i < productes.length; i++) {
            System.out.printf("El producte %s val %.2f€\n", productes[i].getName(), productes[i].getPrize());
        }
    }
}
