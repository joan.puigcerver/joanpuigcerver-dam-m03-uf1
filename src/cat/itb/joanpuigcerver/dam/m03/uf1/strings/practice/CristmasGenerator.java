package cat.itb.joanpuigcerver.dam.m03.uf1.strings.practice;

import java.util.Locale;
import java.util.Scanner;

public class CristmasGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String data_abans = scanner.next();
        String data_despres = scanner.next();
        String felicitat_abans = scanner.next();
        String felicitat_despres = scanner.next();
        String escriptor_abans = scanner.next();
        String escriptor_despres = scanner.next();
        while(true){
            String line = scanner.nextLine();
            if(line.equals("END"))
                break;
            line = line.replace(data_abans, data_despres);
            line = line.replace(felicitat_abans, felicitat_despres);
            line = line.replace(escriptor_abans, escriptor_despres);
            System.out.println(line);
        }
    }
}
