package cat.itb.joanpuigcerver.dam.m03.uf1;

import java.util.Scanner;

public class Escacs
{
    static final char NEGRE = '█';
    public static void main(String[] args)
    {
        Scanner kbd = new Scanner(System.in);
        int n = kbd.nextInt();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                char c = (i + j) % 2 == 0 ? NEGRE : ' ';
                System.out.print(c);
                System.out.print(c);
            }
            System.out.println();
        }
    }
}
