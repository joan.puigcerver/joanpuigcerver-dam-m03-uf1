package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n = scanner.nextInt();
        int[] valors = new int[n];
        for(int i = 0; i < valors.length; i++){
            valors[i] = scanner.nextInt();
        }

        boolean isOrdered = true;
        for(int i = 1; i < valors.length; i++){
            if(valors[i-1] > valors[i])
                isOrdered = false;
        }

        if(isOrdered)
            System.out.println("ordenats");
        else
            System.out.println("desordenats");
    }
}
