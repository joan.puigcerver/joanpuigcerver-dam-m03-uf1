package cat.itb.joanpuigcerver.dam.m03.uf1.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class GenderGuesser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String actual = scanner.next();
        while (!actual.equals("END")){
            char lastChar = actual.charAt(actual.length() - 1);

            if(lastChar == 'a')
                System.out.println("femení");
            else if(lastChar == 's')
                System.out.println("plural");
            else
                System.out.println("masculí");

            actual = scanner.next();
        }
    }
}
