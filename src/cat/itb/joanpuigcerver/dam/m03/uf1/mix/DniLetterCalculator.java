package cat.itb.joanpuigcerver.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class DniLetterCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        char[] lletresDNI = {
                'T', 'R', 'W', 'A', 'G', 'M', 'Y',
                'F', 'P', 'D', 'X', 'B', 'N', 'J',
                'Z', 'S', 'U', 'V', 'H', 'L', 'C',
                'K', 'E'
        };

        int dni = scanner.nextInt();
        int residu = dni % 23;
        char lletra = lletresDNI[residu];
        System.out.printf("%d%c\n", dni, lletra);
    }
}