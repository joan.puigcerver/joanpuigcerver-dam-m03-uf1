package cat.itb.joanpuigcerver.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> llista = new ArrayList<Integer>();

        int numActual = scanner.nextInt();
        while(numActual != -1)
        {
            // afegir el nombre
            if(llista.size() % 2 == 0)
                llista.add(0, numActual);
            else
                llista.add(numActual);
            numActual = scanner.nextInt();
        }
        System.out.println(llista);
    }
}
