package cat.itb.joanpuigcerver.dam.m03.uf1.arraysdin;

import java.util.*;

public class PassarLlista {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<String> alumnes = new ArrayList<String>(Arrays.asList(
                "Magalí"
                , "Magdalena"
                , "Magí"
                , "Manel"
                , "Manela"
                , "Manuel"
                , "Manuela"
                , "Mar"
                , "Marc"
                , "Margalida"
                , "Marçal"
                , "Marcel"
                , "Maria"
                , "Maricel"
                , "Marina"
                , "Marta"
                , "Martí"
                , "Martina"
        ));
        List<Integer> indexAssistents = new ArrayList<Integer>();

        int numActual = scanner.nextInt();
        while (numActual != -1){
            indexAssistents.add(numActual);
            numActual = scanner.nextInt();
        }

        Collections.sort(indexAssistents, Collections.reverseOrder());
        System.out.println(indexAssistents);
        for(int index : indexAssistents)
            alumnes.remove(index);


        System.out.println(alumnes);
    }
}
