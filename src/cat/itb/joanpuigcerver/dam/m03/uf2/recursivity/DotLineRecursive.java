package cat.itb.joanpuigcerver.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class DotLineRecursive {
    public static String punts(int n){
        if(n == 0)
            return "";
        else
            return "." + punts(n - 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int nPunts = scanner.nextInt();
        System.out.println(punts(nPunts));
    }
}
