package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class ExamGrade {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nota = scanner.nextInt();

        boolean isGradeValid = nota >= 0 && nota <= 10;
        if(isGradeValid){
            // Nota vàlida
            if(nota >= 9)
                System.out.println("Excelent");
            else if(nota >= 7)
                System.out.println("Notable");
            else if(nota == 6)
                System.out.println("Bé");
            else if(nota == 5)
                System.out.println("Suficient");
            else
                System.out.println("Suspès");
        } else
            System.out.println("Nota invàlida");

    }
}
