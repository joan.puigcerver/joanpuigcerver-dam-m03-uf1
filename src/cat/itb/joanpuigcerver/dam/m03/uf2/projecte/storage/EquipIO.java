package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.storage;

import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Equip;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Lliga;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EquipIO {
    private Path equipsPath;
    private Lliga lliga;

    public EquipIO(String equipsPath){
        this.equipsPath = Path.of(equipsPath);
        this.lliga = Lliga.getInstance();
    }

    public void read(){
        try {
            if(Files.exists(equipsPath)){
                List<Equip> equips = readEquips();

                for(Equip e : equips)
                    lliga.addEquip(e);
            }
        } catch (IOException e){
            System.out.println("Error llegint els equips.");
        }
    }

    public void write(){
        try {
            List<Equip> equips = lliga.getEquips();
            writeEquips(equips);
        }catch (IOException e){
            System.out.println("Error guardant els equips.");
        }
    }

    private void writeEquips(List<Equip> equips) throws IOException {
        OutputStream outputStream = Files.newOutputStream(equipsPath, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        PrintStream printStream = new PrintStream(outputStream, true);

        printStream.println(equips.size());
        for(Equip e : equips){
            printStream.printf("%d %s %s\n", e.getId(), e.getNom(), e.getAbreviacio());
        }
    }

    private List<Equip> readEquips() throws IOException {
        Scanner scannerFitxer = new Scanner(equipsPath);

        List<Equip> equips = new ArrayList<Equip>();
        int mida = scannerFitxer.nextInt();

        for(int i = 0; i < mida; i++){
            int id = scannerFitxer.nextInt();
            String nom = scannerFitxer.next();
            String abreviacio = scannerFitxer.next();
            equips.add(new Equip(id, nom, abreviacio));
        }

        return equips;
    }
}
