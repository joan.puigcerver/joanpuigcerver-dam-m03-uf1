package cat.itb.joanpuigcerver.dam.m03.uf5.regex;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountPlurals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String text = scanner.nextLine();
        Pattern pluralPattern = Pattern.compile("\\b\\w*s\\b");
        Matcher matcher = pluralPattern.matcher(text);

        int count = 0;
        while (matcher.find()){
            count++;
            System.out.println(matcher.group());
        }
        System.out.println(count);
    }
}
