package cat.itb.joanpuigcerver.dam.m03.uf4.abstracts.comparable;

import cat.itb.joanpuigcerver.dam.m03.uf4.abstracts.interficies.Rectangle;

import java.util.*;

public class TestComparable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> nombres = new ArrayList<Integer>(
                Arrays.asList(1, 6, 8, 2, 9, 5, 0));

        List<Rectangle> rectangles = new ArrayList<Rectangle>();
        rectangles.add(new Rectangle(2, 5));
        rectangles.add(new Rectangle(1, 3));
        rectangles.add(new Rectangle(3, 2));
        rectangles.add(new Rectangle(2, 2));

        System.out.println(nombres);
        int min = Collections.min(nombres);
        System.out.println("Minim: " + min);
        System.out.println(rectangles);
        Rectangle minRectangle = Collections.min(rectangles);
        System.out.println("Minim: " + minRectangle);

        Collections.sort(rectangles);
        System.out.println(rectangles);

        Comparator<Rectangle> heightComparator = new Comparator<Rectangle>(){
            @Override
            public int compare(Rectangle r1, Rectangle r2){
                return Double.compare(r1.getHeight(), r2.getHeight());
            }
        };
        Collections.sort(rectangles, heightComparator);
        System.out.println(rectangles);

        Comparator<Rectangle> widthComparator = (r1, r2) -> Double.compare(r1.getWidth(), r2.getWidth());
        Collections.sort(rectangles, widthComparator);
        System.out.println(rectangles);
    }
}
