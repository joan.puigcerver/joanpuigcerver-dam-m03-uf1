package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.List;

public class TestPersona {
    // MAIN
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Persona profe = new Persona("Joan", "Puigcerver", 26, 'M');
        Persona alume = new Persona("Pep", "Martínez", 20, 'M');

        System.out.printf("El profe %s té %d anys\n", profe.getNom(), profe.getEdat());
        System.out.printf("El alumne %s té %d anys\n", alume.getNom(), alume.getEdat());

        profe.setEdat(30);
        System.out.printf("El profe %s ara té %d anys\n", profe.getNom(), profe.getEdat());

        List<Persona> persones = new ArrayList<Persona>();
        persones.add(profe);
    }
}
