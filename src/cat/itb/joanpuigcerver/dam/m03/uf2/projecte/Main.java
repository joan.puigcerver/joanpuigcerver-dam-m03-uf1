package cat.itb.joanpuigcerver.dam.m03.uf2.projecte;

import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Equip;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Lliga;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.storage.EquipIO;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.ui.MenuPrincipal;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.ui.MyScanner;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        MyScanner scanner = new MyScanner();
        Lliga lliga = Lliga.getInstance();
        // lliga.addEquip("Valencia", "VLC");
        // lliga.addEquip("Barcelona", "BCN");
        // lliga.addEquip("Madrid", "MAD");
        // lliga.addPartit(0, 1, 3, 2);
        // lliga.addPartit(1, 2, 4, 1);

        EquipIO equipIO = new EquipIO("/home/joapuiib/m3/equips.txt");
        equipIO.read();

        MenuPrincipal menuPrincipal = new MenuPrincipal(scanner);
        menuPrincipal.mostrarMenu();

        equipIO.write();
    }
}
