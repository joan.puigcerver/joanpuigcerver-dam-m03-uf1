package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class MatrixBoxesOpenedCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[][] countOpened = new int[4][4];
        System.out.println(Arrays.deepToString(countOpened));

        int fila = scanner.nextInt();
        while(fila != -1){
            int columna = scanner.nextInt();

            countOpened[fila][columna]++;
            System.out.println(Arrays.deepToString(countOpened));

            fila = scanner.nextInt();
        }
        System.out.println(Arrays.deepToString(countOpened));
    }
}
