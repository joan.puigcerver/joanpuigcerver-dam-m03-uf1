package cat.itb.joanpuigcerver.dam.m03.uf2.classfun;

import java.util.Locale;
import java.util.Scanner;

public class BasicRobot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Robot robot = new Robot();
        while(true){
            String accio = scanner.next();

            if(accio.equals("END"))
                break;

            switch (accio){
                case "DALT":
                    robot.moveUp();
                    break;
                case "BAIX":
                    robot.moveDown();
                    break;
                case "DRETA":
                    robot.moveRight();
                    break;
                case "ESQUERRA":
                    robot.moveLeft();
                    break;
                case "ACCELERAR":
                    robot.speedUp();
                    break;
                case "DISMINUIR":
                    robot.slowDown();
                    break;
                case "POSICIO":
                    robot.printPosition();
                    break;
                case "VELOCITAT":
                    robot.printSpeed();
                    break;
                default:
                    System.out.println("Acció no definida.");
                    break;
            }
        }
    }
}
