package cat.itb.joanpuigcerver.dam.m03.uf2.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class LibertySpeechApp {
    public static int casosTotals(Pais[] paisos){
        int suma = 0;
        for(Pais p : paisos)
            suma += p.getCasos();
        return suma;
    }

    public static Pais getPaisMaxCasos(Pais[] paisos){
        Pais max = paisos[0];
        for(int i = 1; i < paisos.length; i++){
            if(paisos[i].getCasos() > max.getCasos())
                max = paisos[i];
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nombrePaisos = scanner.nextInt();
        Pais[] paisos = new Pais[nombrePaisos];
        for(int i = 0; i < nombrePaisos; i++)
            paisos[i] = Pais.readPais(scanner);

        System.out.println("---------- Paisos ----------");
        for (Pais p : paisos)
            System.out.printf("%s - puntuació: %.1f\n", p, p.getPuntuacio());
        System.out.println("---------- Resum ----------");
        System.out.printf("Casos totals: %d\n", casosTotals(paisos));
        System.out.printf("País amb més casos: %s\n", getPaisMaxCasos(paisos).getNom());
    }
}
