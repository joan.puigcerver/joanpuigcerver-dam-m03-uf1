package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        int i = 1;
        while(i < 10){
            int resultat = i * n;
            System.out.printf("%d * %d = %d\n", i, n, resultat);
            i++;
        }

    }
}
