package cat.itb.joanpuigcerver.dam.m03.uf6.projecte.data;

import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.exeptions.RectangleNotFoundException;
import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.models.Rectangle;
import org.w3c.dom.css.Rect;

import java.sql.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;


public class RectangleDAO {
    public void insert(Rectangle r){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO rectangles(width, height) VALUES(?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, r.getWidth());
            insertStatement.setInt(2, r.getHeight());
            insertStatement.execute();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public List<Rectangle> list(){
        List<Rectangle> list = new ArrayList<>();
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM rectangles";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            while (resultat.next()){
                int id = resultat.getInt("id");
                int width = resultat.getInt("width");
                int height = resultat.getInt("height");
                list.add(new Rectangle(id, width, height));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    public boolean delete(int id){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "DELETE FROM rectangles WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean update(Rectangle r){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE rectangles SET(width, height) = (?, ?) WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, r.getWidth());
            insertStatement.setInt(2, r.getHeight());
            insertStatement.setInt(3, r.getId());
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public Rectangle get(int id) throws RectangleNotFoundException{
        Rectangle r = null;
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM rectangles WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            ResultSet result = insertStatement.executeQuery();
            if(!result.next())
                throw new RectangleNotFoundException("No s'ha trobat cap rectangle amb la id " + id);

            int width = result.getInt("width");
            int height = result.getInt("height");
            r = new Rectangle(id, width, height);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return r;
    }
}
