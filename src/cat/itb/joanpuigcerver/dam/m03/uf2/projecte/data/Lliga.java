package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data;

import java.util.ArrayList;
import java.util.List;

public class Lliga {
    private static Lliga lliga = null;

    public static Lliga getInstance(){
        if(lliga == null)
            lliga = new Lliga();

        return lliga;
    }


    List<Equip> equips;
    List<Partit> partits;

    private Lliga(){
        equips = new ArrayList<Equip>();
        partits = new ArrayList<Partit>();
    }

    public List<Equip> getEquips(){
        return equips;
    }

    public int getNombreEquips(){
        return equips.size();
    }
    public int getNombrePartits(){
        return partits.size();
    }
    public int getNombrePartits(int equipID){
        int count = 0;
        for(Partit p : partits){
            if(p.getEquipVisitant().getId() == equipID ||
               p.getEquipLocal().getId() == equipID)
                count++;
        }
        return count;
    }

    public Equip addEquip(String nom, String abreviacio){
        int id = equips.size();
        Equip e = new Equip(id, nom, abreviacio);
        equips.add(e);
        return e;
    }

    public Equip addEquip(Equip e){
        equips.add(e);
        return e;
    }

    public void llistaEquips(){
        for(Equip e : equips){
            System.out.printf("%d. %s\n", e.getId(), e);
        }
    }

    public Equip getEquipById(int id){
        for(Equip e : equips){
            if(e.getId() == id){
                return e;
            }
        }
        return null;
    }

    public void addPartit(int idLocal, int idVisitant, int golsLocal, int golsVisitant){
        Equip equipLocal = getEquipById(idLocal);
        Equip equipVisitant = getEquipById(idVisitant);
        Partit p = new Partit(equipLocal, equipVisitant, golsLocal, golsVisitant);
        partits.add(p);
    }

    public void llistaPartits(){
        int comptador = 0;
        for(Partit p : partits){
            System.out.printf("%d. %s\n", comptador++, p);
        }
    }

    public int totalGols(){
        int suma = 0;
        for(Partit p : partits){
            suma += p.getGolsTotals();
        }
        return suma;
    }
    public int totalGols(int equipID){
        int suma = 0;
        for(Partit p : partits){
            if(p.getEquipLocal().getId() == equipID)
                suma += p.getGolsLocals();
            else if(p.getEquipVisitant().getId() == equipID)
                suma += p.getGolsVisitants();
        }
        return suma;
    }

    public double mitjanaGols(){
        int gols = totalGols();
        int nombrePartits = getNombrePartits();
        double mitjana = ((double) gols) / nombrePartits;
        return mitjana;
    }
    public double mitjanaGols(int equipID){
        int gols = totalGols(equipID);
        int nombrePartits = getNombrePartits(equipID);
        double mitjana = ((double) gols) / nombrePartits;
        return mitjana;
    }
}
