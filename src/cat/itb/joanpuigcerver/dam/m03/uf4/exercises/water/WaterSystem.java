package cat.itb.joanpuigcerver.dam.m03.uf4.exercises.water;

import java.util.List;

public interface WaterSystem {
    List<Double> getHumidityRecord();
    void startWaterSystem();
}
