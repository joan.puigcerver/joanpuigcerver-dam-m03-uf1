package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NextSecond {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int hores = scanner.nextInt();
        int minuts = scanner.nextInt();
        int segons = scanner.nextInt();

        System.out.printf("Són les %02d:%02d:%02d\n", hores, minuts, segons);

        segons += 1;

        boolean isSegonsValid = segons < 60;
        if( ! isSegonsValid ){
            segons = 0;

            minuts += 1;
            if (minuts >= 60) {
                minuts = 0;

                hores += 1;
                if(hores >= 24){
                    hores = 0;
                }
            }
        }

        System.out.printf("Són les %02d:%02d:%02d\n", hores, minuts, segons);
        System.out.println("Són les "+ hores +":"+ minuts +":"+ segons);
    }
}
