package cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces;

import cat.itb.joanpuigcerver.dam.m03.uf4.practica.Position;

public class King extends ChessPiece{
    public King(boolean white){
        super(white);
    }

    @Override
    protected String getPieceString() {
        return "♚";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return false;
    }
}
