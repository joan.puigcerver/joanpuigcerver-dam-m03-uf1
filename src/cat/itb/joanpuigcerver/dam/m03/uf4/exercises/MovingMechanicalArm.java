package cat.itb.joanpuigcerver.dam.m03.uf4.exercises;

public class MovingMechanicalArm extends MechanicalArm {
    protected double position;

    public MovingMechanicalArm(){
        super();
        this.position = 0;
    }

    public double getPosition() {
        return position;
    }
    protected void setPosition(double position) {
        if(isTurnedOn())
            this.position = Math.max(position, 0);
    }

    public void move(double distance){
        setPosition(getPosition() + distance);
    }

    @Override
    public String toString() {
        return "MovingMechanicalArm{" +
                "angle=" + angle +
                ", height=" + height +
                ", turnedOn=" + turnedOn +
                ", position=" + position +
                '}';
    }
}