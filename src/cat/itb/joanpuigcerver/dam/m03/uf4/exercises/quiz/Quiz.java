package cat.itb.joanpuigcerver.dam.m03.uf4.exercises.quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Quiz {
    public static int countCorrect(List<Question> questions){
        int count = 0;
        for(Question q : questions){
            if(q.isCorrect())
                count++;
        }
        return count;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Question> questions = new ArrayList<>();

        questions.add(new FreeTextQuestion("És el mateix una Interfície que una classe abstrcta?", "No"));
        questions.add(new FreeTextQuestion("Què és millor, un while o un for?", "Depén"));
        String[] opcions = {"4", "5", "1"};
        questions.add(new MultipleChoiceQuestion("Quin és parell?", opcions, 1));

        System.out.println("--------------");
        for(Question q : questions){
            q.printQuestion();
            q.askAnswer(scanner);
            System.out.println("--------------");
        }

        int countCorrect = countCorrect(questions);
        System.out.println("Respostes correctes: " + countCorrect);
    }
}
