package cat.itb.joanpuigcerver.dam.m03.uf1.mix;

import java.util.*;

public class TenguiFalti {

    public static List<Integer> llegirLlista(Scanner scanner){
        List<Integer> llista = new ArrayList<Integer>();
        int num = scanner.nextInt();
        while (num != -1){
            llista.add(num);
            num = scanner.nextInt();
        }
        return llista;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> repetits = llegirLlista(scanner);
        // System.out.println(repetits);
        List<Integer> falta = llegirLlista(scanner);
        // System.out.println(falta);

        List<Integer> possiblesCanvis = new ArrayList<Integer>();
        // for(int i = 0; i < repetits.size(); i++){
        //    int r = repetits.get(i);
        for(int r : repetits){
            for(int f : falta){
                if(r == f)
                    possiblesCanvis.add(r);
            }
        }
        System.out.println(possiblesCanvis);
    }
}