package cat.itb.joanpuigcerver.dam.m03.uf6.projecte.models;

public class Rectangle {
    private int id;
    private int width;
    private int height;

    public Rectangle(int id, int width, int height) {
        this.id = id;
        this.width = width;
        this.height = height;
    }
    public Rectangle(int width, int height) {
        this.id = -1;
        this.width = width;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getArea(){
        return width * height;
    }

    public int getPermieter(){
        return 2 * (width + height);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "id=" + id +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
