package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WhichPizzaShouldIBuy {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        // Pizza redona
        double diametre = scanner.nextDouble();
        double radi = diametre / 2;
        double areaRedona = Math.PI * Math.pow(radi, 2);
        System.out.printf("Area pizza redona: %.2f\n", areaRedona);

        // Pizza rectangular
        double base = scanner.nextDouble();
        double altura = scanner.nextDouble();
        double areaRect = base * altura;
        System.out.printf("Area pizza rectangular: %.2f\n", areaRect);

        if(areaRedona > areaRect)
            System.out.println("Compra la rodona");
        else
            System.out.println("Compra la rectangular");
    }
}
