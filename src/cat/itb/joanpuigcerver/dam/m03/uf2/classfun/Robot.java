package cat.itb.joanpuigcerver.dam.m03.uf2.classfun;

public class Robot {
    private double x;
    private double y;
    private double speed;

    public Robot(){
        x = 0;
        y = 0;
        speed = 1;
    }

    public void moveUp(){
        this.y += speed;
    }
    public void moveDown(){
        this.y -= speed;
    }
    public void moveRight(){
        this.x += speed;
    }
    public void moveLeft(){
        this.x -= speed;
    }

    public void speedUp(){
        this.speed += 0.5;

        if(speed > 10)
            speed = 10;
    }
    public void slowDown(){
        this.speed -= 0.5;

        if(speed < 0)
            speed = 0;
    }

    public void printPosition(){
        System.out.printf("La posició del robot és (%.1f, %.1f)\n", x, y);
    }
    public void printSpeed(){
        System.out.printf("La velocitat del robot és %.1f\n", speed);
    }
}
