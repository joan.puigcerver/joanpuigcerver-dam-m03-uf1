package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class MinOf10Values {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[] valors = new int[10];
        for(int i = 0; i < valors.length; i++)
            valors[i] = scanner.nextInt();

        int minValue = valors[0];
        for(int i = 0; i < valors.length; i++){
            if(valors[i] < minValue)
                minValue = valors[i];
        }

        System.out.println(minValue);
    }
}
