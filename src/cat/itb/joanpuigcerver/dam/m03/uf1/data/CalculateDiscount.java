package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Scanner;
import java.util.Locale;

public class CalculateDiscount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double preuOriginal = scanner.nextDouble();
        double preuFinal = scanner.nextDouble();

        double descompte = (1 - preuFinal / preuOriginal)*100;

        System.out.printf("%.2f\n",descompte);
    }
}
