package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IntRectangleArea {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int base = scanner.nextInt();
        int altura = scanner.nextInt();
        int area = base * altura;

        System.out.println(area);
    }
}
