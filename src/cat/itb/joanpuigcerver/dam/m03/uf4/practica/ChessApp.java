package cat.itb.joanpuigcerver.dam.m03.uf4.practica;

import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.Bishop;
import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.ChessPiece;
import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.King;

import java.util.Locale;
import java.util.Scanner;

public class ChessApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        ChessBoard board = new ChessBoard();
        while(true){
            board.printBoard();

            String from = scanner.next();

            if(from.equals("-1"))
                break;

            String to = scanner.next();
            board.move(from, to);
        }
    }
}
