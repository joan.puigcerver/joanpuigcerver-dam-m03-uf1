package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class ThreeCardPoker {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        // c1 <= c2 <= c3
        int c1 = scanner.nextInt();
        int c2 = scanner.nextInt();
        int c3 = scanner.nextInt();

        if(c1 == c2 && c2 == c3)
            System.out.println("Trio");
        else if(c1 == c2 || c2 == c3)
            System.out.println("Parella");
        else if(c1 + 1 == c2 && c2 + 1 == c3)
            System.out.println("Escala");
        else
            System.out.println("Número alt");

    }
}
