package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class BoxesOpenedCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[] countOpened = new int[11];
        System.out.println(Arrays.toString(countOpened));

        int numActual = scanner.nextInt();
        while(numActual != -1)
        {
            // actualitzar el contador de la caixa
            // countOpened[numActual] = countOpened[numActual] + 1;
            // countOpened[numActual] += 1;
            countOpened[numActual]++;

            System.out.println(Arrays.toString(countOpened));
            numActual = scanner.nextInt();
        }
        System.out.println(Arrays.toString(countOpened));
    }
}
