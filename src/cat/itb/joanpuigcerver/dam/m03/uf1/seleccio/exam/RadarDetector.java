package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class RadarDetector {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int velocitat = scanner.nextInt();

        if(velocitat <= 120)
            System.out.println("Correcte");
        else if(velocitat <= 140)
            System.out.println("Multa lleu");
        else
            System.out.println("Multa greu");

        if(velocitat > 140)
            System.out.println("Multa greu");
        else if(velocitat > 120)
            System.out.println("Multa lleu");
        else
            System.out.println("Correcte");
    }
}
