package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class OnlyVowels {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int nChar = scanner.nextInt();

        for(int i = 0; i < nChar; i++){
            String lletraStr = scanner.next();
            char lletraChar = lletraStr.charAt(0);

            switch (lletraStr){
                case "a":
                case "e":
                case "i":
                case "o":
                case "u":
                    System.out.println(lletraStr);
                    break;
            }
        }
    }
}

