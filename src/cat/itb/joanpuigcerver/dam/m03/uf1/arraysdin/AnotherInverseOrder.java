package cat.itb.joanpuigcerver.dam.m03.uf1.arraysdin;

import java.util.*;

public class AnotherInverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> valors = new ArrayList<Integer>();
        System.out.println(valors);

        int numActual = scanner.nextInt();
        while(numActual != -1)
        {
            valors.add(0, numActual);
            System.out.println(valors);
            numActual = scanner.nextInt();
        }

        for(int valor : valors){
            System.out.print(valor);
            System.out.print(" ");
        }
    }
}
