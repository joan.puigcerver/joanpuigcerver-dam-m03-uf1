package cat.itb.joanpuigcerver.dam.m03.uf6.generalexam;

import cat.itb.joanpuigcerver.dam.m03.uf5.generalexam.Book;
import cat.itb.joanpuigcerver.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.joanpuigcerver.dam.m03.uf6.generalexam.data.Database;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class InsertBookApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Database db = Database.getInstance();
        BookDAO bookDAO = new BookDAO();
        db.connect();

        String title = scanner.nextLine();
        String author = scanner.nextLine();
        String isbn = scanner.nextLine();
        int year = scanner.nextInt();
        int pages = scanner.nextInt();
        Book b = new Book(title, author, isbn, pages, year);
        boolean ok = bookDAO.insert(b);
        if(ok)
            System.out.printf("Llibre %s insertat correctament.\n", b);

        db.close();
    }
}
