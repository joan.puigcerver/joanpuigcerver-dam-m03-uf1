package cat.itb.joanpuigcerver.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class NPometes{
    public static String getAppleSongStanza(int applesCount){
        return String.format("%1$d pometes té el pomer,%nde %1$d una, de %1$d una,%n" +
                "%1$d pometes té el pomer,%nde %1$d una en caigué.%n%nSi mireu el vent d'on vé%n" +
                "veureu el pomer com dansa,%nsi mireu el vent d'on vé%n" +
                "veureu com dansa el pomer.%n", applesCount);
    }

    public static void printCanço(int n){
        if(n == 0)
            System.out.println(getAppleSongStanza(n));
        else{
            System.out.println(getAppleSongStanza(n));
            printCanço(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        printCanço(4);
    }
}
