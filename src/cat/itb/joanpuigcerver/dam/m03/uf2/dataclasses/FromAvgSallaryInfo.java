package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FromAvgSallaryInfo {
    public static double computeTotalSallary(Treballador[] treballadors){
        int suma = 0;
        for(Treballador t : treballadors){
            suma += t.getSou();
        }
        return suma;
    }
    public static double computeAverageSallary(Treballador[] treballadors){
        double average = (double)(computeTotalSallary(treballadors)) / treballadors.length;
        return average;
    }

    public static List<Treballador> findBelowSallary(Treballador[] treballadors, double sallary){
        List<Treballador> belowSallarytreballadors = new ArrayList<>();
        for(Treballador t : treballadors) {
            if(t.getSou() < sallary)
                belowSallarytreballadors.add(t);
        }
        return belowSallarytreballadors;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nTreballadors = scanner.nextInt();
        Treballador[] treballadors = new Treballador[nTreballadors];

        for (int i = 0; i < treballadors.length; i++) {
            int sou = scanner.nextInt();
            String nom = scanner.next();
            Treballador t = new Treballador(nom, sou);
            treballadors[i] = t;
        }

        double average = computeAverageSallary(treballadors);
        System.out.printf("Average: %.1f\n", average);

        List<Treballador> belowSallary = findBelowSallary(treballadors, average);
        for( Treballador t : belowSallary){
            System.out.println(t.getNom());
        }
    }
}
