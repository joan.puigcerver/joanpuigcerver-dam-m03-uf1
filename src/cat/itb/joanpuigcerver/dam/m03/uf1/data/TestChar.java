package cat.itb.joanpuigcerver.dam.m03.uf1.data;

public class TestChar {
    public static void main(String[] args) {
        char inicialNom = 'J';
        char inicialCognom = 'P';

        boolean sonIguals = inicialNom == inicialCognom;

        System.out.println("Incial nom: " + inicialNom );
        System.out.println("Incial cognom: " + inicialCognom );

        System.out.println("Son iguals? " + sonIguals);
    }
}
