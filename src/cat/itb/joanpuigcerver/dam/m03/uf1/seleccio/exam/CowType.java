package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class CowType {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int edat = scanner.nextInt();
        // 1 = mascle, 2 = femella
        int sexe = scanner.nextInt();
        // 1 = no capat, 2 = capat
        int capat = scanner.nextInt();

        if(edat < 2)
            System.out.println("vedell");
        else {
            // Adult
            if(sexe == 2)
                System.out.println("vaca");
            else{
                // Mascle
                if(capat == 1)
                    System.out.println("toro");
                else
                    System.out.println("bou");
            }
        }

    }
}
