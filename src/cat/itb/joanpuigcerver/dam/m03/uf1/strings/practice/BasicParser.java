package cat.itb.joanpuigcerver.dam.m03.uf1.strings.practice;

import java.util.Locale;
import java.util.Scanner;

public class BasicParser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        while(true){
            String line = scanner.nextLine();
            if(line.equals("END"))
                break;
            line = line.replace("**","\u001B[1m"); // Negreta
            line = line.replace("--","\u001B[3m"); // Cursiva
            line = line.replace("++","\u001B[0m"); // Reset Format

            line = line.replace("//r//","\u001B[31m"); // Vermell
            line = line.replace("//g//","\u001B[32m"); // Verd
            line = line.replace("//b//","\u001B[34m"); // Blau
            line = line.replace("//s//","\u001B[39m"); // Reset color

            System.out.println(line);
        }
    }
}
