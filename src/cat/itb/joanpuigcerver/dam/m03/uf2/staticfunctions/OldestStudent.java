package cat.itb.joanpuigcerver.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class OldestStudent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        int maxim = IntegerLists.max(list);
        System.out.printf("L'aulmne més gran té %d anys\n", maxim);
    }
}

