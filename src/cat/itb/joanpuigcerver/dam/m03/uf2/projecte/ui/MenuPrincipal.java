package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.ui;

import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Lliga;

public class MenuPrincipal {
    MyScanner scanner;
    MenuDades menuDades;
    MenuEstadistiques menuEstadistiques;

    public MenuPrincipal(MyScanner scanner){
        this.scanner = scanner;
        menuDades = new MenuDades(scanner);
        menuEstadistiques = new MenuEstadistiques(scanner);
    }

    public void mostrarMenu(){
        while(true) {
            System.out.println("Benvinguts a GESTOR LLIGA 2020/2021");
            System.out.println("Quina operació vols realitzar?");
            System.out.println("1) Dades");
            System.out.println("2) Estadístiques");
            System.out.println("0) Sortir");
            int accio = scanner.nextIntMax(2);

            System.out.println();
            switch (accio) {
                case 1:
                    // MenuDades.mostrarMenu(scanner);
                    menuDades.mostrarMenu();
                    break;
                case 2:
                    menuEstadistiques.mostrarMenu();
                    break;
                case 0:
                    return;
            }
        }
    }
}
