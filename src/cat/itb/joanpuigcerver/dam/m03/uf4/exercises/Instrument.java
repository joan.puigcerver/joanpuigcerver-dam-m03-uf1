package cat.itb.joanpuigcerver.dam.m03.uf4.exercises;

public abstract class Instrument{
    protected String sound;

    public Instrument(){
        this.sound = getSound();
    }

    abstract String getSound();

    public void makeSound(){
        System.out.println(this.sound);
    }
}