package cat.itb.joanpuigcerver.dam.m03.uf5.collections.stack;

import java.util.*;

public class Cards {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Stack<String> stack = new Stack<>();

        while(true){
            String name = scanner.nextLine();
            if(name.equals("END"))
                break;
            else if (name.equals("NEXT")){
                if(stack.isEmpty())
                    System.out.println("No hi ha cartes a la pila.");
                else {
                    String next = stack.pop();
                    System.out.println("Next: " + next);
                }
            }else
                stack.push(name);

            System.out.println(stack);
        }
    }
}
