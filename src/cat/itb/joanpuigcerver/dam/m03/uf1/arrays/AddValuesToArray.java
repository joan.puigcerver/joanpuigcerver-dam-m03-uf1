package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class AddValuesToArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double[] valors = new double[50];

        valors[0] = 31.0;
        valors[1] = 56.0;
        valors[19] = 12.0;
        valors[valors.length - 1] = 79.0;

        System.out.println(Arrays.toString(valors));
    }
}
