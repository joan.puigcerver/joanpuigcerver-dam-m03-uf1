package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class ManualPow {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int base = scanner.nextInt();
        int exponent = scanner.nextInt();

        int resultat = 1;
        for(int i = 0; i < exponent; i++){
            resultat *= base;
        }
        System.out.println(resultat);
    }
}
