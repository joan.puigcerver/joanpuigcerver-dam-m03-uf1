package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class MatrixElementSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[][] matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}};
        for(int i = 0; i < matrix.length; i++)
            System.out.println(Arrays.toString(matrix[i]));

        int sumaTotal = 0;
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                sumaTotal += matrix[i][j];
            }
        }
        System.out.println(sumaTotal);
    }
}
