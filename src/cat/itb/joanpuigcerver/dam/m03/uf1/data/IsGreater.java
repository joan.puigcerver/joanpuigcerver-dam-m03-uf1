package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Scanner;
import java.util.Locale;

public class IsGreater {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int primerNombre = scanner.nextInt();
        int segonNombre = scanner.nextInt();

        boolean isGreater = primerNombre > segonNombre;
        System.out.println(isGreater);
    }
}
