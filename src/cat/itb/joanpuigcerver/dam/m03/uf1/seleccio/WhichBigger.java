package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WhichBigger {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();

        int maxim = 0;
        if(n1 > n2)
            maxim = n1;
        else
            maxim = n2;

        System.out.println(maxim);
    }
}
