package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.ui;

import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Equip;
import cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data.Lliga;

public class MenuEstadistiques {
    MyScanner scanner;
    Lliga lliga;

    public MenuEstadistiques(MyScanner scanner) {
        this.scanner = scanner;
        this.lliga = Lliga.getInstance();
    }

    public void mostrarMenu() {
        while (true) {
            System.out.println("Menu ESTADISTIQUES");
            System.out.println("Quina operació vols realitzar?");
            System.out.println("1) Total gols i mitjana gols per partit.");
            System.out.println("2) Total gols i mitjana gols per partit d'un equip.");
            System.out.println("0) Sortir");
            int accio = scanner.nextIntMax(2);

            switch (accio) {
                case 1:
                    totalGols();
                    break;
                case 2:
                    totalGolsEquip();
                    break;
                case 0:
                    return;
            }
        }
    }

    public void totalGols(){
        int totalGols = lliga.totalGols();
        double mitjanaGols = lliga.mitjanaGols();
        System.out.printf("S'han marcat %d gols en la lliga.\n", totalGols);
        System.out.printf("La mitjana de gols per partit és %.2f\n.", mitjanaGols);
    }
    public void totalGolsEquip(){
        lliga.llistaEquips();
        System.out.print("Introudeix el equip: ");
        int equipID = scanner.nextInt();

        int totalGols = lliga.totalGols(equipID);
        double mitjanaGols = lliga.mitjanaGols(equipID);
        System.out.printf("S'han marcat %d gols en la lliga.\n", totalGols);
        System.out.printf("La mitjana de gols per partit és %.2f\n.", mitjanaGols);
    }
}
