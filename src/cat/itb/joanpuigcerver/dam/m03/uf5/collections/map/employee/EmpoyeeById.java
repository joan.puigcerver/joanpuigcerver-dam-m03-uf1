package cat.itb.joanpuigcerver.dam.m03.uf5.collections.map.employee;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class EmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<String, Employee> employees = new HashMap<>();

        int employeeCount = scanner.nextInt();
        scanner.nextLine();

        for(int i = 0; i < employeeCount; i++){
            Employee e = Employee.readEmployee(scanner);
            employees.put(e.getId(), e);
        }

        while (true){
            String id = scanner.next();
            if(id.equals("END"))
                break;

            Employee employee = employees.get(id);
            System.out.println(employee == null ? "No existeix aquest empleat." : employee);
        }
    }
}
