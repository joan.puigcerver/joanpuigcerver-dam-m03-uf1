package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.examen;

import java.util.*;

public class PairsAtTheEnd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> llista = new ArrayList<Integer>();

        int numActual = scanner.nextInt();
        while(numActual != -1){

            if(numActual % 2 == 0)
                llista.add(numActual);
            else
                llista.add(0, numActual);

            numActual = scanner.nextInt();
        }

        System.out.println(llista);
    }
}
