package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int contador = 0;

        String linia = scanner.nextLine();
        while(!linia.equals("END")){
            contador++;
            linia = scanner.nextLine();
        }

        System.out.println(contador);
    }
}
