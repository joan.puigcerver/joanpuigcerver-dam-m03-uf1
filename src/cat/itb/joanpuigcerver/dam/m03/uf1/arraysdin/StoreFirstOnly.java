package cat.itb.joanpuigcerver.dam.m03.uf1.arraysdin;

import java.util.*;

public class StoreFirstOnly {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> llista = new ArrayList<Integer>();

        int numActual = scanner.nextInt();
        while(numActual != -1){

            // Busque si numActual està a la llista
            if(!llista.contains(numActual))
                llista.add(numActual);

            System.out.println(llista);
            numActual = scanner.nextInt();
        }

        System.out.println(llista);
    }
}
