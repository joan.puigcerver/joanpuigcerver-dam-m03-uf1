package cat.itb.joanpuigcerver.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class MultiplicationRecursive {
    public static int M(int a, int b){
        if(b == 0)
            return 0;
        else
            return a + M(a, b-1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(M(a, b));
    }
}
