package cat.itb.joanpuigcerver.dam.m03.uf2.classfun;

import cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses.Product;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.List;

public class BuyList {
    List<Product> llistaProductes;

    public BuyList(){
        llistaProductes = new ArrayList<Product>();
    }

    public void addProduct(Product p){
        llistaProductes.add(p);
    }

    @Override
    public String toString() {
        return "BuyList{" +
                "llistaProductes=" + llistaProductes +
                '}';
    }

    public double totalPrize(){
        double total = 0;
        for(Product p : llistaProductes){
            total += p.totalPrize();
        }
        return total;
    }

    public void printTicket(){
        System.out.println("-------- Compra --------");
        for(Product p : llistaProductes){
            System.out.printf("%s - %.2f\n", p, p.totalPrize());
        }
        System.out.println("-------------------------");
        System.out.printf("Total: %.2f\n", this.totalPrize());
        System.out.println("-------------------------");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        BuyList llista = new BuyList();

        int nProductes = scanner.nextInt();
        for (int i = 0; i < nProductes; i++) {
            Product p = Product.readProduct(scanner);
            llista.addProduct(p);
            System.out.println(llista);
        }

        llista.printTicket();
    }
}
