package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class MultiplyTableFull {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        for(int i = 1; i < 10; i++) {
            for(int j = 1; j < 10; j++) {
                if(j>1)
                    System.out.print(" ");
                System.out.printf("%2d", i*j);
            }
            System.out.println();
        }
    }
}
