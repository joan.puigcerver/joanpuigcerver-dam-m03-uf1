package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class GoTournamentGreatestScore {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String nomGuanyador = null;
        int puntuacioMaxima = -1;

        String nomActual = scanner.nextLine();
        while(!nomActual.equals("END")){
            int puntuacioActual = scanner.nextInt();
            scanner.nextLine(); // Consumir final de línia

            if(puntuacioActual > puntuacioMaxima){
                puntuacioMaxima = puntuacioActual;
                nomGuanyador = nomActual;
            }
            nomActual = scanner.nextLine();
        }
        System.out.printf("%s: %d\n", nomGuanyador, puntuacioMaxima);
    }
}
