package cat.itb.joanpuigcerver.dam.m03.uf2.classfun;

import java.util.Locale;
import java.util.Scanner;

public class CampSiteOrganizer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Camping camping = new Camping();
        while (true){
            String accio = scanner.next();
            if (accio.equals("END"))
                break;
            else if(accio.equals("ENTRA")){
                int nPersones = scanner.nextInt();
                String nom = scanner.next();

                camping.addReserva(nom, nPersones);
            } else if(accio.equals("MARXA")){
                String nom = scanner.next();
                camping.removeReserva(nom);
            }
            camping.printInfo();
        }
    }
}
