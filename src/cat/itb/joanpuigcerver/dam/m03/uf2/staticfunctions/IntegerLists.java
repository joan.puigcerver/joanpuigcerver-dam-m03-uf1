package cat.itb.joanpuigcerver.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Classe que proporciona mètodes per treballar amb Llistes d'enters
 */
public class IntegerLists {
    /**
     * Llegeix una llista d'enters de l'entrada estàndar mitjançant un Scanner
     * Es llegiran tots els enters fins trobar un -1.
     *
     * @param scanner Objecte scanner que llegirà l'entrada
     * @return Llista d'enters
     */
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<Integer>();

        while(true){
            int num = scanner.nextInt();
            if(num == -1)
                break;
            list.add(num);
        }

        return list;
    }

    /**
     * Retorna el valor mínim de la llista introduïda
     * @param list Llista d'enters
     * @return Valor mínim
     */
    public static int min(List<Integer> list){
        int minim = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if(list.get(i) < minim){
                minim = list.get(i);
            }
        }
        return minim;
    }

    /**
     * Retorna el valor màxim de la llista introduïda
     * @param list Llista d'enters
     * @return Valor màxim
     */
    public static int max(List<Integer> list){
        int maxim = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if(list.get(i) > maxim){
                maxim = list.get(i);
            }
        }
        return maxim;
    }

    /**
     * Retorna la suma de tots els elements de la llista
     * @param list
     * @return
     */
    public static int sum(List<Integer> list){
        int suma = 0;
        for(int n : list){
            suma += n;
        }
        return suma;
    }

    /**
     * Calcula la mitjana (amb decimals) dels valors de la llista
     * @param list Llista d'enters
     * @return Mitjana dels valors amb decimals
     */
    public static double average(List<Integer> list){
        int suma = sum(list);
        double average = ((double) suma) / list.size();
        return average;
    }

    public static void main(String[] args) {
        int a; // 0
        boolean b; // false
        String p; // null
    }
}
