package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class CountDown {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        for(int i = n; i > 0; i--){
            System.out.print(i);
        }

        // Salt de línia final
        System.out.println();

    }
}
