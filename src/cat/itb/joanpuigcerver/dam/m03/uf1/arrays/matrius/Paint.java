package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.matrius;

/**
 *  Fer un programa que imprimeixi un menú per poder pintar.
 *  Les opcions seran les següents:
 *      1. Pintar linia horitzontal.
 *      2. Pintar linia vertical.
 *      3. Pintar linia oblicua.
 *      4. Pintar rectangle.
 *      5. Pintar triangle.
 *      6. Pintar triangle invertit.
 *      0. Sortir
 *      
 *      Després de cada opció, el programa ha d'imprimir la matriu.
 */

public class Paint {
    
    public static void main(String[] args) {

        PrintArray print_array = new PrintArray(640,640);
        print_array.setDrawText(false);
        print_array.setDrawColor(true);
        print_array.setFontSize(10);
        
        int [][] matriu = new int[100][100];
        
        print_array.printArray(matriu);
        
    }
}
