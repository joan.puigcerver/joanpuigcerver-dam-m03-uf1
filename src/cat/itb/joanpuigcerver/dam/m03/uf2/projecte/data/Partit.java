package cat.itb.joanpuigcerver.dam.m03.uf2.projecte.data;

public class Partit {
    private int id;
    private Equip equipLocal;
    private Equip equipVisitant;
    private int golsLocals;
    private int golsVisitants;

    public Partit(Equip equipLocal, Equip equipVisitant, int golsLocals, int golsVisitants) {
        this.equipLocal = equipLocal;
        this.equipVisitant = equipVisitant;
        this.golsLocals = golsLocals;
        this.golsVisitants = golsVisitants;
    }

    public Equip getEquipLocal() {
        return equipLocal;
    }
    public Equip getEquipVisitant() {
        return equipVisitant;
    }
    public int getGolsLocals() {
        return golsLocals;
    }
    public int getGolsVisitants() {
        return golsVisitants;
    }

    public void setEquipLocal(Equip equipLocal) {
        this.equipLocal = equipLocal;
    }
    public void setEquipVisitant(Equip equipVisitant) {
        this.equipVisitant = equipVisitant;
    }
    public void setGolsLocals(int golsLocals) {
        this.golsLocals = golsLocals;
    }
    public void setGolsVisitants(int golsVisitants) {
        this.golsVisitants = golsVisitants;
    }

    @Override
    public String toString() {
        return String.format("%s %d - %d %s", equipLocal.getNom(), golsLocals, golsVisitants, equipVisitant.getNom());
    }

    public int getGolsTotals(){
        return golsLocals + golsVisitants;
    }
}
