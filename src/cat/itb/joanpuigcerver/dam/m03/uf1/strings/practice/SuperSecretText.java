package cat.itb.joanpuigcerver.dam.m03.uf1.strings.practice;

import java.util.Locale;
import java.util.Scanner;

public class SuperSecretText {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int xifrar = scanner.nextInt();
        int offset = scanner.nextInt();
        if (xifrar == 0)
            offset = -offset;

        scanner.nextLine(); // Netejar /n final
        String linia = scanner.nextLine();
        String encriptat = "";
        for(int i = 0; i < linia.length(); i++){
            // char nouChar = (char) (linia.charAt(i) + xifrar == 1 ? offset : - offset);
            char nouChar = (char) (linia.charAt(i) + offset);
            encriptat+= nouChar;
        }
        System.out.println(encriptat);
    }
}
