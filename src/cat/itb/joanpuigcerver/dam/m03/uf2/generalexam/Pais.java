package cat.itb.joanpuigcerver.dam.m03.uf2.generalexam;

import java.util.Scanner;

public class Pais {
    private String nom;
    private int casos;
    private double gravetat;

    public Pais(String nom, int casos, double gravetat) {
        this.nom = nom;
        this.casos = casos;
        this.gravetat = gravetat;
    }

    public static Pais readPais(Scanner scanner){
        String nom = scanner.next();
        int casos = scanner.nextInt();
        double gravetat = scanner.nextDouble();
        return new Pais(nom, casos, gravetat);
    }

    public String getNom() {
        return nom;
    }

    public int getCasos() {
        return casos;
    }

    public double getGravetat() {
        return gravetat;
    }

    public double getPuntuacio(){
        return casos * gravetat;
    }

    @Override
    public String toString(){
        return String.format("%s - casos: %d - gravetat: %.1f", nom, casos, gravetat);
    }
}
