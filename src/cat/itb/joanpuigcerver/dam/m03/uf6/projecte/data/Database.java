package cat.itb.joanpuigcerver.dam.m03.uf6.projecte.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private static String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/";
    private static String BD = "ywfigkss";
    private static String USER = "ywfigkss";
    private static String PASS = "3MthmecCn4KwklFuRqxugJDKk7ofohmr";

    private static Database database = null;
    public static Database getInstance(){
        if(database == null)
            database = new Database();
        return database;
    }

    private Connection connection;

    public Database(){
        connection = null;
    }
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void close(){
        try {
            connection.close();
        }catch(SQLException e){
            System.err.println("Error tancant la BD");
        }
        connection = null;
    }
}
