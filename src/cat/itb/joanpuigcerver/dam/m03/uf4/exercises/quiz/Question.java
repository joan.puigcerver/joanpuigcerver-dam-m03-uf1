package cat.itb.joanpuigcerver.dam.m03.uf4.exercises.quiz;

import java.util.Scanner;

public interface Question {
    void printQuestion();
    void askAnswer(Scanner scanner);
    boolean isCorrect();
}
