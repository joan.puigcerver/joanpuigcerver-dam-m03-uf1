package cat.itb.joanpuigcerver.dam.m03.uf5.collections.queue;

import com.sun.security.jgss.GSSUtil;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

public class Supermarket {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Queue<String> queue = new LinkedList<>();

        while(true){
            String name = scanner.next();
            if(name.equals("END"))
                break;
            else if (name.equals("NEXT")){
                String next = queue.poll();
                if(next == null)
                    System.out.println("No hi ha clients a la cua");
                else
                    System.out.println("Next: " + next);
            }else
                queue.add(name);

            System.out.println(queue);
        }
    }
}
