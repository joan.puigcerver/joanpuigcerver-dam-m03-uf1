package cat.itb.joanpuigcerver.dam.m03.uf6.projecte;

import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.data.Database;
import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.ui.MenuPrincipal;

import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Database.getInstance().connect();

        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuPrincipal menuPrincipal = new MenuPrincipal(scanner);
        menuPrincipal.showMenu();

        Database.getInstance().close();
    }
}
