package cat.itb.joanpuigcerver.dam.m03.uf2.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class AirContamination {
    public static int[] readArray(Scanner scanner){
        int mida = scanner.nextInt();

        int[] array = new int[mida];
        for (int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt();

        return array;
    }

    public static double average(int[] array){
        int suma = 0;
        for(int n : array)
            suma += n;

        return ((double) suma) / array.length;
    }

    public static int countAbove(int[] array, double threshold){
        int count = 0;
        for(int n : array){
            if(n > threshold)
                count++;
        }

        return count;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int[] contaminacio = readArray(scanner);

        double average = average(contaminacio);
        System.out.printf("Contaminació mitjana: %.2f\n", average);

        int count = countAbove(contaminacio, average);
        System.out.printf("Dies amb contaminació superior: %d\n", count);
    }
}
