package cat.itb.joanpuigcerver.dam.m03.uf2.staticfunctions;

import java.util.Scanner;
import java.util.Locale;

public class RepositoriName {
    // nom, cognom, 3, 2 => nomcognom-m03-uf2
    public static String generarNomRespositori(String nom, String cognom, int modul, int uf){
        nom = nom.toLowerCase();
        cognom = cognom.toLowerCase();
        String repositori = String.format("%s%s-m%02d-uf%d", nom, cognom, modul, uf);
        return repositori;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String nom = scanner.next();
        String cognom = scanner.next();

        String repositori = generarNomRespositori(nom, cognom, 3, 2);
        String repositori2 = generarNomRespositori(nom, cognom, 16, 1);

        System.out.println(repositori);
        System.out.println(repositori2);
    }
}
