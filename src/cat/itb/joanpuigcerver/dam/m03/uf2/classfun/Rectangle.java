package cat.itb.joanpuigcerver.dam.m03.uf2.classfun;

import java.util.Scanner;
import java.util.Locale;

public class Rectangle {
    public static Rectangle readRectangle(Scanner scanner){
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();
        return new Rectangle(width, height);
    }

    private double width;
    private double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }
    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }
    public void setHeight(double height) {
        this.height = height;
    }

    public double getArea(){
        return width * height;
    }

    public double getPerimeter(){
        return 2 * (width + height);
    }

    public String getRectangleInfo(){
        return String.format("Un rectangle de %.2f x %.2f té %.2f d'area i %.2f de perímetre.", this.getWidth(), this.getHeight(), this.getArea(), this.getPerimeter());
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nRectangles = scanner.nextInt();
        Rectangle[] rectangles = new Rectangle[nRectangles];

        for (int i = 0; i < rectangles.length; i++) {
            rectangles[i] = Rectangle.readRectangle(scanner);
        }

        for (int i = 0; i < rectangles.length; i++) {
            System.out.println(rectangles[i].getRectangleInfo());
        }
    }
}
