package cat.itb.joanpuigcerver.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class BasketballDecriptor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> punts = new ArrayList<Integer>();
        int num = scanner.nextInt();
        while(num != -1){
            punts.add(num);
            num = scanner.nextInt();
        }

        int[] contador = new int[3];
        int anterior = 0;
        for ( int p : punts ){
            int diferencia = p - anterior;

            contador[diferencia - 1]++;

            anterior = p;
        }
        System.out.printf("cistelles d'un punt: %d\n", contador[0]);
        System.out.printf("cistelles de dos punts: %d\n", contador[1]);
        System.out.printf("cistelles de tres punts: %d\n", contador[2]);
    }
}