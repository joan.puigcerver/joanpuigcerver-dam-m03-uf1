package cat.itb.joanpuigcerver.dam.m03.uf2.generalexam;

public class Avistament {
    private Animal animal;
    private int nombre;

    public Avistament(Animal animal, int nombre) {
        this.animal = animal;
        this.nombre = nombre;
    }

    public Animal getAnimal() {
        return animal;
    }

    public int getNombre() {
        return nombre;
    }

    @Override
    public String toString(){
        return String.format("%s: %d", animal, nombre);
    }

}
