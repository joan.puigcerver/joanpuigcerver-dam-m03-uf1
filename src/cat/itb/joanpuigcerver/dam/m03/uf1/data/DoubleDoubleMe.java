package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class DoubleDoubleMe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double primerValor = scanner.nextDouble();
        double doubleValor = primerValor * 2;

        System.out.printf("%.2f\n", doubleValor);
    }
}
