package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.examen;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[] array = new int[10];

        int posicio = scanner.nextInt();
        while(posicio != -1){
            int valor = scanner.nextInt();

            array[posicio] = valor;

            posicio = scanner.nextInt();
        }

        System.out.println(Arrays.toString(array));
    }
}
