package cat.itb.joanpuigcerver.dam.m03.uf6.projecte.ui;

import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.data.RectangleDAO;
import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.exeptions.RectangleNotFoundException;
import cat.itb.joanpuigcerver.dam.m03.uf6.projecte.models.Rectangle;

import java.util.List;
import java.util.Scanner;

public class MenuPrincipal extends Menu{
    private RectangleDAO rectangleDAO;

    public MenuPrincipal(Scanner scanner) {
        super(scanner);
        rectangleDAO = new RectangleDAO();
        addOption("Llista rectangles.");
        addOption("Insertar rectangle.");
        addOption("Editar rectangle.");
        addOption("Eliminar rectangle.");
        addOption("Get rectangle.");
    }

    @Override
    public void action(int selected) {
        switch (selected){
            case 1:
                listRectangles();
                break;
            case 2:
                insertRectangle();
                break;
            case 3:
                updateRectangle();
                break;
            case 4:
                deleteRectangle();
                break;
            case 5:
                getRectangle();
                break;
        }
        System.out.println();
    }

    public void listRectangles(){
        System.out.println("RECTANGLES:");
        List<Rectangle> rectangles = rectangleDAO.list();
        rectangles.forEach(x -> System.out.printf("- %s\n", x));
    }
    public void insertRectangle(){
        System.out.print("Introdueix l'amplada del rectangle: ");
        int width = scanner.nextInt();
        System.out.print("Introdueix l'altura del rectangle: ");
        int height = scanner.nextInt();
        rectangleDAO.insert(new Rectangle(width, height));
    }
    public void updateRectangle(){
        listRectangles();
        System.out.print("Introdueix la id del rectangle: ");
        int id = scanner.nextInt();
        System.out.print("Introdueix l'amplada del rectangle: ");
        int width = scanner.nextInt();
        System.out.print("Introdueix l'altura del rectangle: ");
        int height = scanner.nextInt();
        rectangleDAO.update(new Rectangle(id, width, height));
    }
    public void deleteRectangle(){
        listRectangles();
        System.out.print("Introdueix id: ");
        int id = scanner.nextInt();
        rectangleDAO.delete(id);
    }

    public void getRectangle(){
        listRectangles();
        System.out.print("Introdueix id: ");
        int id = scanner.nextInt();
        try {
            Rectangle r = rectangleDAO.get(id);
            System.out.println(r);
        } catch (RectangleNotFoundException ex){
            System.out.println(ex.getMessage());
        }
    }
}
