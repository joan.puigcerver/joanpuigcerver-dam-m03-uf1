package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IntDoubleMe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int primerValor = scanner.nextInt();

        int doubleValor = primerValor * 2;

        System.out.println(doubleValor);
    }
}
