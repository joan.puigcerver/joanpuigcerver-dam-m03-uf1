package cat.itb.joanpuigcerver.dam.m03.uf1.mix;

import java.util.Locale;
import java.util.Scanner;

public class HomeworkHelper {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        while (true){
            int dividend = scanner.nextInt();
            if(dividend == -1)
                break;

            int divisor = scanner.nextInt();
            int quocient = scanner.nextInt();
            int residu = scanner.nextInt();

            boolean esCorrecte = (quocient * divisor + residu) == dividend;
            System.out.println(esCorrecte ? "correcte" : "error");
        }
    }
}
