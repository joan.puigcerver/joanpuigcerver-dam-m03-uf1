package cat.itb.joanpuigcerver.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class ArrayMaxValueRecursive {

    private static int recursiveMax(int[] a, int i){
        if(i == a.length - 1)
            return a[i];
        else
            return Math.max(a[i], recursiveMax(a, i+1));
    }
    public static int recursiveMax(int[] a){
        return recursiveMax(a, 0);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.println(recursiveMax(a));
    }
}
