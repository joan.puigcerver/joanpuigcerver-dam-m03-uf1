package cat.itb.joanpuigcerver.dam.m03.uf2.staticfunctions;

import java.util.Locale;
import java.util.Scanner;
import java.util.List;

public class IntListReaderSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        System.out.println(list);
        List<Integer> list2 = IntegerLists.readIntegerList(scanner);
        System.out.println(list2);
    }
}
