package cat.itb.joanpuigcerver.dam.m03.uf2.generalexam;

public class Animal {
    private String nomComu;
    private String nomCientific;

    public Animal(String nomComu, String nomCientific) {
        this.nomComu = nomComu;
        this.nomCientific = nomCientific;
    }

    public String getNomComu() {
        return nomComu;
    }

    public String getNomCientific() {
        return nomCientific;
    }

    @Override
    public String toString(){
        return String.format("%s - %s", nomCientific, nomComu);
    }
}
