package cat.itb.joanpuigcerver.dam.m03.uf1.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class BasketballTournamentManager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int[] victories = new int[8];

        int actual = scanner.nextInt();
        while(actual != -1){
            victories[actual]++;
            System.out.printf("L'equip %d té %d victòries\n", actual, victories[actual]);
            actual = scanner.nextInt();
        }

        int guanyador = 0;
        for(int i = 1; i < victories.length; i++){
            if(victories[i] > victories[guanyador]){
                guanyador = i;
            }
        }
        System.out.printf("L'equip guanyador és el %d\n", guanyador);
    }
}
