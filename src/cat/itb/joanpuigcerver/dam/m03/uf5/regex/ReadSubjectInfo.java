package cat.itb.joanpuigcerver.dam.m03.uf5.regex;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadSubjectInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        Pattern pattern = Pattern.compile("(\\w+)-M(\\d+)UF(\\d+)");
        for (int i = 0; i < n; i++) {
            String codi = scanner.next();
            Matcher matcher = pattern.matcher(codi);
            if(matcher.matches()){
                String cicle = matcher.group(1);
                int modul = Integer.parseInt(matcher.group(2));
                int uf = Integer.parseInt(matcher.group(3));
                System.out.printf("Estàs curstant la unitat formativa %d, del mòdul %d de %s\n", uf, modul, cicle);
            }else{
                System.out.println("Codi incorrecte");
            }
        }
    }
}
