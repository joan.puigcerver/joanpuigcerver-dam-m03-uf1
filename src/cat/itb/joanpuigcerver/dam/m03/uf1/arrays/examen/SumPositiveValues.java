package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.examen;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class SumPositiveValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int mida = scanner.nextInt();
        int[] array = new int[mida];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int sumaPositius = 0;
        for (int i = 0; i < array.length; i++) {
            if(array[i] > 0)
                sumaPositius += array[i];
        }
        System.out.println(sumaPositius);
    }
}
