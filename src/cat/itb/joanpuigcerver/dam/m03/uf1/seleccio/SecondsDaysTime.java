package cat.itb.joanpuigcerver.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class SecondsDaysTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int segonsTotals = scanner.nextInt();

        int minutsTotals = segonsTotals / 60;
        int segonsRestants = segonsTotals % 60;

        int horesTotals = minutsTotals / 60;
        int minutsRestants = minutsTotals % 60;


        System.out.printf("Segons totals: %d\n", segonsTotals);
        System.out.printf("Segons restants: %d\n", segonsRestants);
        System.out.printf("Minuts totals: %d\n", minutsTotals);
    }
}
