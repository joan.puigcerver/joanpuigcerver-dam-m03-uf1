package cat.itb.joanpuigcerver.dam.m03.uf4.abstracts.interficies;

public class Rectangle implements Figure,Comparable<Rectangle> {
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    public double getWidth() {
        return width;
    }
    public double getHeight() {
        return height;
    }

    @Override
    public double area(){
        return width * height;
    }

    @Override
    public double perimeter(){
        return 2 * (width + height);
    }

    @Override
    public int compareTo(Rectangle that){
        return Double.compare(this.area(), that.area());
    }
}
