package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.examen;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class ChessRockMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        boolean[][] tauler = new boolean[8][8];
        int fila = scanner.nextInt();
        int columna = scanner.nextInt();

        for (int i = 0; i < tauler.length; i++) {
            // Horitzontal
            tauler[fila][i] = true;

            // Vertical
            tauler[i][columna] = true;
        }
        tauler[fila][columna] = false;

        System.out.println(Arrays.deepToString(tauler));
    }
}
