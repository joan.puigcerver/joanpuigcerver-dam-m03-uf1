package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Scanner;
import java.util.Locale;

public class HowBigIsMyPizza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double diametre = scanner.nextDouble();
        double radi = diametre / 2;
        double superficieRodona = Math.PI * Math.pow(radi, 2);
        System.out.printf("Superficie rodona (r: %.2f): %.2f\n", diametre/2, superficieRodona);

        double base = scanner.nextDouble();
        double altrura = scanner.nextDouble();
        double superficieRect = base * altrura;
        System.out.printf("Superficie rectangular (%.2f x %.2f): %.2f\n", base, altrura, superficieRect);

        boolean isBigger = superficieRodona > superficieRect;
        System.out.println(isBigger);

    }
}
