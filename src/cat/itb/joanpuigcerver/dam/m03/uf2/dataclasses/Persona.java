package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import java.util.*;

public class Persona {
    // Attributs de la classe Persona
    private String nom;
    private String cognom;
    private int edat;
    private char sexe;
    private String nacionalitat;

    /**
     * Constructor persona
     * @param nom Nom de la persona
     * @param cognom Cognom de la persona
     * @param edat Edat (enter)
     * @param sexe Sexe (M = Masculí, F = Femení)
     */
    public Persona(String nom, String cognom, int edat, char sexe){
        this.nom = nom;
        this.cognom = cognom;
        this.edat = edat;
        this.sexe = sexe;
        this.nacionalitat = "Espanyola";
    }

    // getter
    public String getNom(){
        return this.nom;
    }
    public String getCognom(){
        return this.cognom;
    }
    public int getEdat(){
        return this.edat;
    }
    public char getSexe(){
        return this.sexe;
    }
    public String getNacionalitat() {
        return this.nacionalitat;
    }

    // setter
    public void setNom(String nom){
        this.nom = nom;
    }
    public void setEdat(int edat){
        this.edat = edat;
    }
}
