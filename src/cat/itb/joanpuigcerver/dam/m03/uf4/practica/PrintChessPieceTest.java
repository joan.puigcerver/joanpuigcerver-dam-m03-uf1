package cat.itb.joanpuigcerver.dam.m03.uf4.practica;

import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.Bishop;
import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.ChessPiece;
import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.King;
import cat.itb.joanpuigcerver.dam.m03.uf4.practica.pieces.Rook;

public class PrintChessPieceTest {
    public static void main(String[] args) {
        ChessPiece white = new King(true);
        ChessPiece black = new Bishop(false);
        System.out.println(white);
        System.out.println(black);
    }
}
