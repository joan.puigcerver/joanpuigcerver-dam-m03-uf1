package cat.itb.joanpuigcerver.dam.m03.uf4.abstracts.interficies;

public interface Figure {
    /**
     *
     * @return
     */
    public double area();
    public double perimeter();
}
