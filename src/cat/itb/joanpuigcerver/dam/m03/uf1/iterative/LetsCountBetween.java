package cat.itb.joanpuigcerver.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class LetsCountBetween {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int lower = scanner.nextInt();
        int upper = scanner.nextInt();

        for(int i = lower + 1; i < upper; i++){
            System.out.print(i);
        }

        // Salt de línia final
        System.out.println();

    }
}
