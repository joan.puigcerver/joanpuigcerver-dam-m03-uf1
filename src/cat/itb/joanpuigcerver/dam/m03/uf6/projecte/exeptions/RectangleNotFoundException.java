package cat.itb.joanpuigcerver.dam.m03.uf6.projecte.exeptions;

public class RectangleNotFoundException extends Exception{
    public RectangleNotFoundException(){
        super();
    }
    public RectangleNotFoundException(String message){
        super(message);
    }
}
