package cat.itb.joanpuigcerver.dam.m03.uf1.data;

import java.util.Scanner;
import java.util.Locale;

public class CrazyOperation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        int n3 = scanner.nextInt();
        int n4 = scanner.nextInt();

        int resulatCrazyOperation = (n1 + n2) * (n3 - n4);
        System.out.println(resulatCrazyOperation);
    }
}
