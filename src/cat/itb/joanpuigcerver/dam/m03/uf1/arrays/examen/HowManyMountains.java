package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.examen;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class HowManyMountains {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double[][] map ={{1.5,1.6,1.8,1.7,1.6}
                        ,{1.5,2.6,2.8,2.7,1.6}
                        ,{1.5,4.6,4.4,4.9,1.6}
                        ,{2.5,1.6,3.8,7.7,3.6}
                        ,{1.5,2.6,3.8,2.7,1.6}};
        for (int i = 1; i < map.length - 1; i++) {
            for (int j = 1; j < map[i].length - 1; j++) {
                boolean esMuntanya = true;
                esMuntanya = esMuntanya && map[i][j-1] <= map[i][j];
                esMuntanya &= map[i][j+1] <= map[i][j];
                esMuntanya &= map[i-1][j] <= map[i][j];
                esMuntanya &= map[i+1][j] <= map[i][j];

                if(esMuntanya)
                    System.out.printf("%d %d: %.1f\n", i, j, map[i][j]);
            }
        }
    }
}
