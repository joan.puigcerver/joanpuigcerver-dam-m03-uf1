package cat.itb.joanpuigcerver.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class SimpleBattleshipResult {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int[][] tauler = new int[7][7];
        tauler[0][0] = 1;
        tauler[0][1] = 1;
        tauler[0][6] = 1;
        tauler[1][2] = 1;
        tauler[1][6] = 1;
        tauler[2][6] = 1;
        tauler[3][1] = 1;
        tauler[3][2] = 1;
        tauler[3][3] = 1;
        tauler[3][6] = 1;
        tauler[4][4] = 1;
        tauler[5][4] = 1;
        tauler[6][0] = 1;

        int fila = scanner.nextInt();
        int columna = scanner.nextInt();

        if(tauler[fila][columna] == 0)
            System.out.println("aigua");
        else
            System.out.println("tocat");
    }
}
