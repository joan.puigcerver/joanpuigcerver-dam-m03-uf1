package cat.itb.joanpuigcerver.dam.m03.uf2.dataclasses;

import java.util.Scanner;
import java.util.Locale;

public class OneProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String productName = scanner.next();
        double productPrize = scanner.nextDouble();

        Product producte = new Product(productName, productPrize);
        System.out.printf("El producte %s val %.2f€\n", producte.getName(), producte.getPrize());
    }
}
