package cat.itb.joanpuigcerver.dam.m03.uf1.arrays.matrius;

public class Exemple {
    
    static int[][] test_color( )
    {
        int[][] estado_actual = new int[100][10];

        for( int i = 0; i < estado_actual.length; i++)
            for( int j = 0; j < estado_actual[i].length; j++)
                estado_actual[i][j] = j*estado_actual.length + i + 1;
        
        return estado_actual;
    }
    
    static int[] colors( )
    {
        // Color.WHITE      = 0
        // Color.BLUE,      = 100
        // Color.YELLOW     = 200
        // Color.RED        = 300
        // Color.GREEN      = 400
        // Color.ORANGE     = 500
        // Color.CYAN       = 600
        // Color.MAGENTA    = 700
        // Color.PINK       = 800
        // Color.LIGHT_GRAY = 900
        // Color.BLACK      = 1000
        
        int[] estado_actual = new int[11];

        for( int i = 0; i < estado_actual.length; i++)
            estado_actual[i] = 100 * i;
        
        return estado_actual;
    }
    
    public static void main(String[] args) {

        PrintArray print_array = new PrintArray(640,640);
        print_array.setDrawText(false);
        print_array.setDrawColor(true);
        print_array.setFontSize(10);
        
        //int [][] matriu = test_color();
        //print_array.printArray(matriu);
        //int [] array = colors();
        //print_array.printArray(array);

        int [][] matriu = new int[100][100];
        
        for( int f = 0; f < matriu.length; f++)
        {
            for( int c = 0; c < matriu[f].length; c++)
            {
                // Diagonal 1
                if ( (f==c) ) matriu[f][c] = 200;
                // Diagonal 2
                if ( (f == matriu[c].length -1 - c) ) matriu[f][c] = 300;
            }
        }
        
        print_array.printArray(matriu);
        
    }
}
